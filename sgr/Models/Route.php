<?php

namespace Sgr\Models;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $table = 'routes';

    protected $primaryKey = 'route_id';

    protected $fillable = ['origin_station_id','destination_station_id'];

    public function originStation()
    {
        return $this->belongsTo(Station::class,'origin_station_id', 'station_id');
    }

    public function destinationStation()
    {
        return $this->belongsTo(Station::class,'destination_station_id', 'station_id');
    }

    public function fare()
    {
        return $this->hasOne(Fare::class, 'route_id', 'route_id');
    }
}
