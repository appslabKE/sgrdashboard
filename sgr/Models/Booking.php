<?php

namespace Sgr\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{

    //pending, booked , cancelled
    const BOOKING_UNSUCCESSFUL = 'pending';
    const BOOKING_SUCCESSFUL = 'booked';
    const BOOKING_CANCELED = 'canceled';

    protected $table = 'bookings';

    //TODO chage fillable;

    protected $fillable = ['trade_id','departure_time','arrival_time','booking_channel','route','date_of_travel',
        'total_passengers','total_children','phone','cron_status','booking_status','total_amount','refunded_amount','created_at','updated_at'];


    public function route()
    {
        return $this->belongsTo(Route::class, 'route_id','route_id');
    }

    public function passengers()
    {
        return $this->hasMany(Passanger::class, 'booking_id', 'booking_id');
    }

    public function payment()
    {
        return $this->hasOne(Payment::class,'booking_id', 'booking_id');
    }

}
