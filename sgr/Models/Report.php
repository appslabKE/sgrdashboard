<?php

namespace Sgr\Models;

//use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model
{
    use SoftDeletes;

   protected $fillable = ['user_id','passenger_id','issue_id','message','action'];

   protected $dates = ['deleted_at'];

    public function issue()
    {
        return $this->belongsTo(Issue::class, 'issue_id', 'id');
   }

    public function passenger()
    {
        return $this->belongsTo(Passanger::class, 'passenger_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
