<?php

namespace Sgr\Models;

use Illuminate\Database\Eloquent\Model;

class TrainCoachAllocation extends Model
{
    protected $table = 'Train_coach_allocations';

    protected $fillable = ['train_id','name','description','class_id','total_seats','seat_layout_json'];


    public function tickets()
    {
        return $this->hasMany(Passanger::class);
    }

}
