<?php

namespace  Sgr\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    const PAYMENT_PENDING = 'pending';
    const PAYMENT_FAILED = 'canceled';
    const PAYMENT_SUCCESSFUL = "paid";

    protected $table = 'payments';
    protected $primaryKey = 'payment_id';

    protected $fillable = ['booking_id','payment_channel','amount','system_transaction_id',
        'provider_transaction_id','payment_status','created_at','updated_at'];

    public function booking()
    {
        return $this->belongsTo(Booking::class, 'booking_id', 'booking_id');
    }
}
