<?php

namespace Sgr\Models;

use Illuminate\Database\Eloquent\Model;

class Passanger extends Model
{
    const TICKET_PLACED = 'pending';
    const TICKET_CONFIRMED = 'booked';
    const TICKET_CANCELED_NO_PAY = 'canceled_pending';
    const TICKET_CANCELED_PAID = 'canceled_booked';
    const TICKET_REFUNDED = 'refunded';

    const FULLFILMENT_PLACED = 'Ticket placed';
    const FULLFILMENT_BOOKED = 'Bus booked';
    const FULLFILMENT_ONTRANSIT = 'On transit';
    const FULLFILMENT_ARRIVED = 'Arrived';


    protected $table = 'passengers';

    protected $fillable = ['booking_id','class','ticket_type','name','id_number','seat_no',
        'ticket_no','ticket_status','created_at','updated_at'];

    public function booking()
    {
        return $this->belongsTo(Booking::class, 'booking_id', 'booking_id');
    }

    public function refund()
    {
        return $this->hasOne(Refund::class, 'passenger_id', 'id');
    }

    public function feedback()
    {
        return $this->hasMany(Refund::class, 'passenger_id', 'id');
    }

    public function bookingClass()
    {
        return $this->belongsTo(TrainClass::class, 'class_id','class_id');
    }
}
