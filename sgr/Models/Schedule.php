<?php

namespace Sgr\Models;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $table = 'schedules';

    public function bookings()
    {
        return $this->hasMany(Booking::class,'schedule_id','schedule_id');
    }
}
