<?php

namespace Sgr\Models;

use Illuminate\Database\Eloquent\Model;

class TrainClass extends Model
{
    protected $table = 'classes';
}
