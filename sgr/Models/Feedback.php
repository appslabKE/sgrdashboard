<?php

namespace Sgr\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = ['passenger_id','user_id','subject','feedback'];

    public function passenger()
    {
        return $this->belongsTo(Passanger::class, 'passenger_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
