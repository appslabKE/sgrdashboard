<?php

namespace Sgr\Models;

use Illuminate\Database\Eloquent\Model;

class Fare extends Model
{
    protected $table = 'fares';

    protected $primaryKey = 'fare_id';

    protected $fillable = ['route_id', 'class_id', 'amount_adult', 'amount_child',
        'active_days'];

    public function route()
    {
        return $this->belongsTo(Route::class, 'route_id','route_id');
    }
}
