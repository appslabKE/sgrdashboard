<?php

namespace  Sgr\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['role', 'description'];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
