<?php

namespace Sgr\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Refund extends Model
{
    const PENDING='Pending';
    const CONFIRMED='Confirmed';

    protected $fillable = ['passenger_id','user_id','refund_method','refund_amount','reason','status'];

    public function passenger()
    {
        return $this->belongsTo(Passanger::class, 'passenger_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
