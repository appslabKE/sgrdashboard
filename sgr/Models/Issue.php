<?php

namespace Sgr\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Issue extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['issue', 'description'];

    public function reports()
    {
        return $this->hasMany(Report::class);
    }
}
