<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 10/26/17
 * Time: 7:56 PM
 */

namespace Sgr\helpers;


use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use PHPMailer\PHPMailer\PHPMailer;

class GeneralHelper
{
    public static function separateDates(Array $combined_dates)
    {
        $dates = explode(' - ', $combined_dates['date_range']);
        $starting_date = Carbon::parse($dates[0]);
        $ending_date = Carbon::parse($dates[1]);

        return ['start_date' => $starting_date, 'ending_date'=>$ending_date];
    }

    public static function sendMail($to, $from, $message,$subject, $api_key)
    {
        $client = new Client();

        $response = $client->post('https://buupass.com/api/mail/send.php', [
            'form_params' => [
                'to' => $to,
                'message' => $message,
                'subject' => $subject,
                'from' => $from,
                'app_key' => $api_key
            ]
        ]);
    }

    public static function getSmsData()
    {
        $client =  new \GuzzleHttp\Client();

        try{

            $response = $client->get('http://pay.kenyasgr.com/balance?pass=secretKenyaSGR');
            $sms_data = json_decode($response->getBody());

            if ($sms_data){

                $sms = [
                    'bal' => $sms_data[0]->UnitsBalance,
                    'expiry_date' => $sms_data[0]->ExpiryDate
                ];

            }

            else{

                $sms = [
                    'bal' => 'unavailable',
                    'expiry_date' => 'unavailable'
                ];

            }
        }

        catch (ClientException $clientException){

            $sms = [
                'bal' => 'unavailable',
                'expiry_date' => 'unavailable'
            ];

        }

        return $sms;
    }
}