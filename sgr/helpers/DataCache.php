<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 10/22/17
 * Time: 3:24 PM
 */

namespace Sgr\helpers;


use Illuminate\Support\Facades\Cache;

class DataCache
{
    public static function putCacheData($key,$data, $minutes)
    {
        Cache::put($key, $data, $minutes);
    }

    public static function getCacheData($key)
    {
        return Cache::get($key);
    }

    public static function putCacheForever($key, $value)
    {
        Cache::forever($key, $value);
    }

}