<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 2/11/17
 * Time: 10:23 AM
 */

namespace Sgr\helpers;
use AfricasTalkingGateway;

require_once('AfricasTalkingGateway.php');


class AThelper
{
    private $gateway;

    /**
     * ATdetails constructor.
     */
    public function __construct()
    {
        $this->gateway = new AfricasTalkingGateway(env('AFRICAISTALKING_USERNAME'),env('AFRICAISTALKING_API_KEY'));
    }

    public function getMessageDetails($recipient,$message)
    {
        $this->sendMessage(['phone_number'=>$recipient,'message'=>$message]);
    }

    private function sendMessage($data)
    {
        try{
            $result = $this->gateway->sendMessage($data['phone_number'], $data['message']);

            foreach ($result as $item)
            {
                if ($item->status != 'Success'){

                    break;

                }
            }

        }

        catch (\AfricasTalkingGatewayException $exception)
        {

        }
    }

    public function validateNumber($phone_number)
    {
        preg_match_all('/^(?:\+\d{2})?\d{10}(?:,(?:\+\d{2})?\d{10})*$/m', $phone_number, $matches, PREG_SET_ORDER,0);

        if (empty($matches)){

            return false;

        }

        return true;
    }


}