<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 8/23/17
 * Time: 2:53 PM
 */

namespace Sgr\Repo;


use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Sgr\helpers\DataCache;
use Sgr\Models\Booking;
use Sgr\Models\Passanger;
use Sgr\Models\Payment;

class InsightsRepo
{
    public function searchedInsights($data)
    {

        if ($data['groupby'] == '%Y-%v'){

            $dateFormat = 'Y-W';

        }

        elseif ($data['groupby'] == '%Y-%m-%d'){

            $dateFormat = 'Y-m-d';
        }

        elseif ($data['groupby'] == '%Y-%M'){

            $dateFormat = 'Y-F';

        }

        else{

            $dateFormat = 'Y';

        }

        $dates = explode(' - ', $data['date_range']);
        $starting_date = Carbon::parse($dates[0])->format($dateFormat);
        $ending_date = Carbon::parse($dates[1])->format($dateFormat);


        if (Cache::has($data['groupby'])){

            $result = DataCache::getCacheData($data['groupby']);

        }

        else{

            $raw_statement = "DATE_FORMAT(bookings.created_at,'" . $data['groupby'] ."') as created_at,bookings.total_amount,
            bookings.booking_channel,passengers.class,payments.payment_status,payments.amount as payment_amount,
                passengers.ticket_status";

            $result = DB::table('bookings')
                ->join('passengers', 'bookings.booking_id', '=', 'passengers.booking_id')
                ->join('payments','bookings.booking_id','=','payments.booking_id')
                ->where('bookings.created_at','>=', Carbon::now()->startOfMonth())
                ->selectRaw($raw_statement)->get();

            DataCache::putCacheData($data['groupby'] , $result, 300);

        }

        $query = $result->where('created_at','>=',$starting_date)->where('created_at','<=',$ending_date);

        $total_booking = count($query);

        $booking_totals = self::getBookingTotals($query);

        $total_revenue_web = $booking_totals->sum('web_paid');
        $total_booking_web = $booking_totals->sum('web_bookings');
        $total_booking_ussd = $booking_totals->sum('ussd_bookings');
        $total_revenue_ussd = $booking_totals->sum('ussd_paid');

        $monthly_records = self::monthlyRecords($booking_totals, $dateFormat);

        $insights  = self::getInsights($booking_totals);

        $net_revenue = collect($insights)->sum('total_revenue');

        return [
            'insights' => $insights,
            'monthly_records' =>$monthly_records,
            'total_booking' => $total_booking,
            'total_revenue_web' => $total_revenue_web,
            'total_revenue_ussd' => $total_revenue_ussd,
            'total_web_booking' => $total_booking_web,
            'total_ussd_booking' => $total_booking_ussd,
            'total_net_revenue' => $net_revenue,
        ];

    }

    public function allInsights()
    {

        if (Cache::has('allInsights')){

            $query = DataCache::getCacheData('allInsights');

        }

        else{

            $query = DB::table('bookings')
                ->join('passengers', 'bookings.booking_id', '=', 'passengers.booking_id')
                ->join('payments','bookings.booking_id','=','payments.booking_id')
                ->where('bookings.created_at','>=',Carbon::now()->startOfMonth())
                ->selectRaw("DATE_FORMAT(bookings.created_at, '%Y-%m-%d') as created_at,bookings.total_amount,
            bookings.booking_channel,passengers.class,payments.payment_status,payments.amount as payment_amount,
                passengers.ticket_status")->get();

            DataCache::putCacheData('allInsights', $query, 300);

        }

        $total_booking = count($query);

        $booking_totals = self::getBookingTotals($query);

        $total_revenue_web = $booking_totals->sum('web_paid');
        $total_booking_web = $booking_totals->sum('web_bookings');
        $total_booking_ussd = $booking_totals->sum('ussd_bookings');
        $total_revenue_ussd = $booking_totals->sum('ussd_paid');

        $monthly_records = self::monthlyRecords($booking_totals);

        $insights  = self::getInsights($booking_totals);

        $net_revenue = collect($insights)->sum('total_revenue');

        return [
            'insights' => $insights,
            'monthly_records' =>$monthly_records,
            'total_booking' => $total_booking,
            'total_revenue_web' => $total_revenue_web,
            'total_revenue_ussd' => $total_revenue_ussd,
            'total_web_booking' => $total_booking_web,
            'total_ussd_booking' => $total_booking_ussd,
            'total_net_revenue' => $net_revenue,
        ];
    }

    public static function getBookingTotals($query)
    {
        return $query->map(function ($value){

            $results = [];
            if ($value->ticket_status == Passanger::TICKET_CONFIRMED ||
                $value->ticket_status == Passanger::TICKET_PLACED){

                $value->booking_channel = empty($value->booking_channel) ? null : $value->booking_channel;
                $results = [
                    'class' => empty($value->class) ? null : $value->class,
                    'date' => $value->created_at,
                    'channel' => $value->booking_channel == null || $value->booking_channel == 0 ? 'USSD' : 'Web' ,
                    'web_paid' => $value->booking_channel == null || $value->booking_channel == 0 ? 0 : $value->payment_amount,
                    'web_bookings' => $value->booking_channel == null || $value->booking_channel == 0 ? 0 : 1,
                    'ussd_paid' => $value->booking_channel == null || $value->booking_channel == 0 ? $value->payment_amount : 0,
                    'ussd_bookings' => $value->booking_channel == null || $value->booking_channel == 0 ? 1 : 0,
                    'amount_paid' => $value->payment_amount
                ];
            }

            return $results;

        })->reject(null);
    }


    public static function monthlyRecords($booking_totals, $dateToShow = 'Y-M')
    {
        $monthly_records = [];

        foreach (collect(self::getMonthlyRecords($booking_totals, $dateToShow))->groupBy('month') as $key => $monthly_data){

            array_push($monthly_records,[
                'month' => $key,
                'total_tickets' =>  $monthly_data->sum('total_tickets'),
                'total_revenue' => $monthly_data->sum('total_revenue'),
                'total_revenue_web' => $monthly_data->sum('total_revenue_web'),
                'channel_web' => $monthly_data->sum('channel_web'),
                'channel_ussd' => $monthly_data->sum('channel_ussd')
            ]);

        };

        return $monthly_records;
    }

    public static function getMonthlyRecords($booking_totals, $dateToShow)
    {
       $records = [];

        foreach ( $booking_totals->groupBy('date') as $key => $month){

//            if (Carbon::parse($key)->year == Carbon::now()->year){

                $channel = [];

                foreach ($month as $item){
                    array_push($channel,[
                        'web' => $item['channel'] != 'USSD' ? 1 : 0,
                        'ussd' => $item['channel'] == 'USSD' ? 1 : 0
                    ]);
                }

                array_push($records,[
                    'month' => $dateToShow == 'Y-M' ? Carbon::parse($key)->format($dateToShow) : $key, //Carbon::parse($key)->format($dateToShow),
                    'total_tickets' =>  count($month),
                    'total_revenue' => $month->sum('amount_paid'),
                    'total_revenue_web' => $month->sum('amount_paid'),
                    'channel_web' => collect($channel)->sum('web'),
                    'channel_ussd' => collect($channel)->sum('ussd')
                ]);
//            }

        }

        return $records;
    }


    public static function getInsights($booking_totals)
    {
        $insights = [];

        foreach ($booking_totals->groupBy('class') as $key => $item){

            array_push($insights, [
                'class' => $key,
                'total_tickets' => count($item),
                'total_revenue' => $item->sum('amount_paid')
            ]);

        }

        return $insights;
    }


    public function misc()
    {
        $query = DB::table('bookings')
            ->join('passengers', 'bookings.booking_id', '=', 'passengers.booking_id')
            ->join('payments','bookings.booking_id','=','payments.booking_id')
            ->where('bookings.created_at','>=', Carbon::now()->startOfMonth())
            ->selectRaw("bookings.created_at,bookings.route,bookings.phone,bookings.total_amount,
            payments.payment_status,payments.payment_channel,payments.amount as payment_amount,
                passengers.ticket_status")->get();

        $routes = [];
        $payments = [];
        $passengers = [];

        foreach ($query->groupBy('route') as $key => $route){

            array_push($routes,[
                'route' => $key,
                'number_of_bookings' => count($route)
            ]);
        }

        foreach ($query->groupBy('payment_channel') as $key => $payment){
            array_push($payments,[
                'method' => $key == 'mpesa' ? 'Mpesa' : 'Others',
                'amount' => $payment->sum('payment_amount')
            ]);
        }

        foreach ($query->groupBy('phone') as $key => $passenger){

            array_push($passengers,[
                'name' => $passenger->first()->phone,
                'number_of_bookings' => count($passenger),
                'amount' => $passenger->sum('total_amount')
            ]);
        }

        $top_payments = collect($payments)->sortByDesc('amount')->take(5);
        $top_passengers = collect($passengers)->sortByDesc('amount')->take(5);

        $top_routes = collect($routes)->sortByDesc('number_of_bookings')->take(5);

        return [
            'top_routes' => $top_routes,
            'top_payments' => $top_payments,
            'top_passengers' => $top_passengers
        ];
    }

}