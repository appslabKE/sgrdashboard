<?php
namespace Sgr\Repo;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use PDO;
use Sgr\Models\Booking;
use Sgr\Models\Payment;
use Sgr\Models\Train_coach_allocation;
use Sgr\Models\TrainCoachAllocation;


/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 8/22/17
 * Time: 5:02 AM
 */
class TicketRepo
{

    public function ticketSearch($data)
    {
        $tickets = [];

        $query_statement = DB::table('bookings')
            ->join('passengers','passengers.booking_id','=', 'bookings.booking_id')
            ->join('payments','payments.booking_id','=', 'bookings.booking_id')
            ->select('bookings.trade_id','bookings.departure_time','bookings.booking_date','bookings.created_at','bookings.arrival_time','bookings.booking_channel','bookings.route','bookings.date_of_travel',
                'bookings.total_passengers','bookings.booking_status','bookings.total_children','bookings.source','bookings.destination','bookings.phone','bookings.refunded_amount',
                'payments.payment_status','payments.payment_channel','payments.amount as payment_amount','payments.payment_status',
                'payments.provider_transaction_id','passengers.class','passengers.ticket_type','passengers.name','passengers.id_number','passengers.seat_no',
                'passengers.ticket_no','passengers.id','passengers.ticket_status','passengers.amount_paid as passenger_payment');

        if (! empty($data['origin_station'])){

            $query_statement = $query_statement->where('bookings.source', '=', $data['origin_station']);

        }

        if (! empty($data['destination_station'])){

            $query_statement = $query_statement->where('bookings.destination', '=', $data['destination_station']);

        }

        if (! empty($data['booking_no'])){

            $query_statement = $query_statement->where('bookings.trade_id', $data['booking_no']);

        }

        if (! empty($data['phone'])){

            $data['phone'] = '254'. substr($data['phone'],1);

            $query_statement = $query_statement->where('bookings.phone','like', '%'.$data['phone'].'%');

        }


//        if (! empty($data['booking_status'])){
//
//            $query_statement = $query_statement->where('booking_status',$data['booking_status']);
//
//        }


        if (! empty($data['travel_date'])){

            $query_statement = $query_statement->whereDate('bookings.date_of_travel', Carbon::parse($data['travel_date'])->toDateString());

        }

        if (! empty($data['booking_date'])){

            $query_statement = $query_statement->whereDate('bookings.booking_date', Carbon::parse($data['booking_date'])->toDateString());

        }

        if (! empty($data['seat_no'])){

            $query_statement = $query_statement->where('passengers.seat_no', $data['seat_no']);

            }

        if (! empty($data['ticket_no'])){

            $query_statement = $query_statement->where('passengers.ticket_no', $data['ticket_no']);

            }

        if (! empty($data['class_id'])){

            $query_statement = $query_statement->where('passengers.class', $data['class_id']);

            }

        if (! empty($data['ticket_status'])){

            $query_statement = $query_statement->where('passengers.ticket_status', $data['ticket_status']);

            }

        if (! empty($data['payment_status'])){

            $query_statement = $query_statement->where('payments.payment_status', $data['payment_status']);

        }

        $statement = $query_statement->getConnection()->getPdo()->prepare($query_statement->toSql());
        $statement->execute($query_statement->getBindings());

        while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
            array_push($tickets,$record);
        }

        return $tickets;

    }

    public function advanceSearch($value, $payment,$data)
    {
        $data_ticket = $value->passengers->map(function ($data_value) use ($value, $payment, $data) {

            $passenger_data = $data_value;

            if (!empty($data['seat_no']))
            {
                $passenger_data = $data_value->seat_no == $data['seat_no'] ? $data_value : null;
            }

            if (!empty($data['ticket_no']))
            {
                $passenger_data = $data_value->ticket_no == $data['ticket_no'] ? $data_value : null;
            }

            if (!empty($data['ticket_status']))
            {
                $passenger_data = $data_value->ticket_status == $data['ticket_status'] ? $data_value : null;
            }

                return $passenger_data != null ? [
                    'ticket_id' => $passenger_data->id,
                    'name' => $passenger_data->name,
                    'national_id' => $passenger_data->id_number,
                    'seat_no' => $passenger_data->seat_no,
                    'ticket_no' => $passenger_data->ticket_no,
//                    'train_no' => $value->train_id,
//                    'coach_no' => $data_value->coach_id,
                    'ticket_status' => $passenger_data->ticket_status,
                    'amount_paid' => $passenger_data->amount_paid,
                    'fulfillment_status' => $passenger_data->fulfillment_status,
                    'booking_id' => $value->trade_id,
                    'travel_date' => $value->date_of_travel,
                    'booking_date' => $value->createdAt,
                    'route' => $value->route,
                    'booking_channel' => $value->booking_channel == 0 ? 'Web' : 'USSD',
                    'ticket_type' => $passenger_data->ticket_type,
//                    'origin_station' => $value->route->originStation->station_name,
//                    'destination_station' => $value->route->destinationStation->station_name,
                    'phone_no' =>$value->phone,
                    'total_fare' => $value->total_amount,
//                    'reporting_time' => $value->reporting_time_mins,
                    'booking_status' => $value->booking_status,
                    'booking_class' => $passenger_data->class,
                    'departure_time' => $value->departure_time,
                    'arrival_time' => $value->arrival_time,
                    'payment_status' => $value->payment->payment_status
                ] : null;

        })->reject(null);

        return $data_ticket;
    }

    public function allTickets()
    {
        $tickets = [];

        $query1 = DB::table('bookings')->join('passengers', 'bookings.booking_id', '=', 'passengers.booking_id')
            ->join('payments','bookings.booking_id','=','payments.booking_id')
            ->select('bookings.trade_id','bookings.departure_time','bookings.booking_date','bookings.created_at','bookings.arrival_time','bookings.booking_channel','bookings.route','bookings.date_of_travel',
                'bookings.total_passengers','bookings.booking_status','bookings.total_children','bookings.source','bookings.destination','bookings.phone','bookings.refunded_amount',
               'payments.payment_status','payments.payment_channel','payments.amount as payment_amount','payments.payment_status',
                'payments.provider_transaction_id','passengers.class','passengers.ticket_type','passengers.name','passengers.id_number','passengers.seat_no',
                'passengers.ticket_no','passengers.id','passengers.ticket_status','passengers.amount_paid as passenger_payment')->orderBy('created_at','desc')->limit(25);

        $statement = $query1->getConnection()->getPdo()->prepare($query1->toSql());
        $statement->execute($query1->getBindings());

        while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
            array_push($tickets,$record);
        }

        return $tickets;

    }
}