<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 10/26/17
 * Time: 4:40 PM
 */

namespace Sgr\Repo;


use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Sgr\helpers\DataCache;
use Sgr\helpers\GeneralHelper;

class ReportRepo
{
    public static function getCustomers($start_date , $ending_date)
    {
        if (Cache::has('customers') && Carbon::parse($start_date)->gt(Carbon::now()->startOfMonth()->subDay(1))){

            $customersRecords = DataCache::getCacheData('customers');

        }

        elseif (Carbon::parse($start_date)->lt(Carbon::now()->startOfMonth())){

            $customersRecords = DB::table('bookings')
                ->join('passengers', 'bookings.booking_id', '=', 'passengers.booking_id')
                ->join('payments', 'bookings.booking_id', '=', 'payments.booking_id')
                ->where('bookings.created_at','>=',Carbon::parse($start_date))
                ->where('bookings.created_at','<=',Carbon::parse($ending_date))
                ->selectRaw("bookings.created_at,bookings.total_amount,passengers.name,passengers.id_number,
                 passengers.ticket_no,bookings.phone, bookings.source, bookings.destination,
                 bookings.date_of_travel,payments.amount as payment_amount")->get();

        }

        else {

            $customersRecords = DB::table('bookings')
                ->join('passengers', 'bookings.booking_id', '=', 'passengers.booking_id')
                ->join('payments', 'bookings.booking_id', '=', 'payments.booking_id')
                ->where('bookings.created_at','>=',Carbon::now()->startOfMonth())
                ->selectRaw("bookings.created_at,bookings.total_amount,passengers.name,passengers.id_number,
                 passengers.ticket_no,bookings.phone, bookings.source, bookings.destination,
                 bookings.date_of_travel,payments.amount as payment_amount")->get();

            DataCache::putCacheData('customers', $customersRecords, 1440);
        }

        return self::getDateRangeData($customersRecords, $start_date, $ending_date);

    }

    public static function getDateRangeData($data, $first_date, $last_date)
    {
        $result = $data->map(function ($values) use($first_date, $last_date){

            if (Carbon::parse($values->created_at)->gt($first_date) && Carbon::parse($values->created_at)->lt($last_date)){
                return $values;
            }

        })->reject(null);

        return $result;
    }


    public static function downloadExcel($date_range)
    {
        $time = time();
        $user = Auth::user()->name;

       $dates = GeneralHelper::separateDates($date_range);

       $data = self::convertResult(self::getCustomers($dates['start_date'], $dates['ending_date']));

        Excel::create('customer_report'.$time.'_'.$user, function ($excel) use($data, $user){
                $excel->sheet($user, function ($sheet) use($data){
                    $sheet->fromArray($data);
                })->export('csv');
            });
    }

    public static function convertResult($data)
    {
        $converted = [];

        foreach ($data as $datum){

            array_push($converted,
                [
                    'Full Name' => $datum->name,
                    'Phone ' => $datum->phone,
                    'National ID' => $datum->id_number,
                    'Origin' => $datum->source,
                    'Destination' => $datum->destination,
                    'Date & Time of Travel' => $datum->date_of_travel,
                    'Fare' => $datum->payment_amount,
                    'Time Booked' => $datum->created_at,
                    'Hops' => ' ',
                    'Ticket Number' => $datum->ticket_no
                ]);

        }


        return $converted;
    }
}