<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 8/23/17
 * Time: 9:50 AM
 */

namespace Sgr\Repo;


use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PDO;
use Sgr\helpers\DataCache;
use Sgr\Models\Booking;
use Sgr\Models\Passanger;
use Sgr\Models\Payment;

class MetricsRepo
{
    public function searchedMetrics($data)
    {
        if ($data['groupby'] == '%Y-%v') {

            $dateFormat = 'Y-W';

        } elseif ($data['groupby'] == '%Y-%m-%d') {

            $dateFormat = 'Y-m-d';
        } elseif ($data['groupby'] == '%Y-%M') {

            $dateFormat = 'Y-F';

        } else {

            $dateFormat = 'Y';

        }

        $dates = explode(' - ', $data['date_range']);
        $starting_date = Carbon::parse($dates[0])->format($dateFormat);
        $ending_date = Carbon::parse($dates[1])->format($dateFormat);

        if (Cache::has($data['groupby'])) {

            $tickets_query = DataCache::getCacheData($data['groupby']);

        } else {

            $query = "DATE_FORMAT(bookings.created_at, '" . $data['groupby'] . "') as created_at,bookings.total_amount,payments.payment_status,payments.amount as payment_amount,passengers.ticket_status ";

            $tickets_query = DB::table('bookings')
                ->join('passengers', 'bookings.booking_id', '=', 'passengers.booking_id')
                ->join('payments', 'bookings.booking_id', '=', 'payments.booking_id')
                ->where('bookings.created_at', '>=', Carbon::now()->startOfYear())
                ->selectRaw($query)->get();

            DataCache::putCacheData($data['groupby'], $tickets_query, 360);

        }


        $result = $tickets_query->where('created_at', '>=', $starting_date)->where('created_at', '<=', $ending_date);

        return self::getMetrics($result);

    }


    public function allMetrics()
    {

        $start_date = DataCache::getCacheData('last_cron_time') == null ?
            Carbon::now()->startOfYear()->subDay(5) : DataCache::getCacheData('last_cron_time');

            $tickets_query = DB::table('bookings')
            ->join('passengers', 'bookings.booking_id', '=', 'passengers.booking_id')
            ->join('payments', 'bookings.booking_id', '=', 'payments.booking_id')
            ->where('cron_status', 0)
            ->whereBetween('bookings.created_at', [ $start_date->subDay(1) , $start_date->addDays(7)])
            ->selectRaw("DATE_FORMAT(bookings.created_at, '%Y%m%d') as created_at, bookings.booking_id,
            bookings.total_amount, bookings.cron_status,payments.payment_status,payments.amount as payment_amount,
                passengers.ticket_status")->get();

        $ids = [];

        foreach ($tickets_query as $key => $ticket){
            array_push($ids,$ticket->booking_id);
        }

        $record = self::getMetrics($tickets_query);

        Booking::whereIn('booking_id', $ids)->update(['cron_status' => 1]);

        DataCache::putCacheForever('last_cron_time', $start_date->addDays(7));

        self::storeMetrics($record['metrics']);

    }

    public static function getMetrics($tickets_query)
    {
        $metrics = $tickets_query->groupBy('created_at')->map(function ($value, $key) {

            $tickets = $value->map(function ($bookings) use ($value, $key) {

                    return [
                        'reserved_ticket' => $bookings->payment_status == Payment::PAYMENT_PENDING ? 1 : 0,
                        'reserved_amount' => $bookings->payment_status == Payment::PAYMENT_PENDING ? $bookings->payment_amount : 0,
                        'canceled_ticket' => $bookings->payment_status == Payment::PAYMENT_FAILED ? 1 : 0,
                        'canceled_amount' => $bookings->payment_status == Payment::PAYMENT_FAILED ? $bookings->payment_amount : 0,
                        'paid_ticket' => $bookings->payment_status == Payment::PAYMENT_SUCCESSFUL ? 1 : 0,
                        'paid_amount' => $bookings->payment_status == Payment::PAYMENT_SUCCESSFUL ? $bookings->payment_amount : 0,
                    ];

            })->reject(null);

            return $tickets->isEmpty() ? null :[
                'date' => $key,
                'reserved_tickets' => $tickets->sum('reserved_ticket'),
                'canceled_tickets' => $tickets->sum('canceled_ticket'),
                'paid_tickets' => $tickets->sum('paid_ticket'),
                'canceled_amount' => $tickets->sum('canceled_amount'),
                'reserved_amount' => $tickets->sum('reserved_amount'),
                'paid_amount' => $tickets->sum('paid_amount'),
            ];

        })->reject(null);

        return [
            'metrics' => $metrics
        ];
    }

    public function storeMetrics($metrics_data)
    {
        foreach ($metrics_data['metrics'] as $key => $metric) {

            $month = substr($key, 0, 6);
            $date = substr($key, 6, 2);

            if (Cache::has($month)) {
                $new_data = [];
                $month_data = DataCache::getCacheData($month);
                if (array_key_exists($date, $month_data)) {

                    foreach ($month_data[$date] as $inner_key => $month_datum) {

                        $new_data[$inner_key] = ($key == 'date' ? $month_datum : ($metric[$inner_key] + $month_datum));
                    }

                    $month_data[$date];
                } else {

                    $month_data[$date] = $metric;
                }

                DataCache::putCacheForever($month, $month_data);

            } else {

                DataCache::putCacheForever($month, [$date => $metric]);
            }

        }
    }
}