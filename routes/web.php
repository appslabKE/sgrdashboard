<?php
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/manage-booking', 'GeneralController@manageBooking');
    Route::get('/search-booking', 'GeneralController@searchBooking');
    Route::post('/search/ticket', 'GeneralController@searchTicket' );
    Route::get('/metrics','GeneralController@metrics');
    Route::get('misc/tops','GeneralController@topMisc');
    Route::get('misc/refunds','GeneralController@showRefunds');
    Route::get('misc/cancelled-tickets','GeneralController@showCancelledTickets');
    Route::resource('misc/feedback','FeedbackController');
    Route::get('/insights','GeneralController@insights');
    Route::resource('/issues-report', 'ReportController');
    Route::get('/home', 'HomeController@index')->name('home');

    //settings
    Route::group(['prefix' => 'settings'], function (){
        Route::resource('users', 'UserController');
        Route::resource('roles', 'RoleController');
        Route::resource('issues', 'IssueController');
    });
//tickets
    Route::group(['prefix' => 'tickets'], function (){
        Route::get('show/{id}','TicketController@show');
    });
//reports
    Route::group(['prefix' => 'reports'], function (){
        Route::get('customers', 'ReportController@customers');
        Route::get('download-excel', 'ReportController@downloadExcel');
    });
//users actions
    Route::group(['prefix' => 'actions'], function (){
        Route::get('cancel-ticket/{id}', 'ActionController@cancelTicket');
        Route::get('re-send-sms/{id}', 'ActionController@resendSms');
        Route::get('refund/{id}', 'ActionController@refund');
        Route::get('re-book/{id}', 'ActionController@reBook');
        Route::get('confirm-refund/{id}', 'ActionController@confirmRefund');
        Route::get('feedback/{id}', 'ActionController@feedback');
        Route::get('confirm-payment/{id}', 'ActionController@confirmPayment');
        Route::post('cancel-ticket/save', 'ActionController@cancelTicketSave');
        Route::post('send-sms/save', 'ActionController@smsSave');
        Route::post('refund/save', 'ActionController@refundSave');
        Route::post('refund/confirm', 'ActionController@refundCustomer');
        Route::post('feedback/save', 'ActionController@feedbacksave');
        Route::post('confirm-payment/save', 'ActionController@confirmPaymentSave');
    });

});

Route::get('clear-cache/{command}','RoleController@clearCache');

Auth::routes();


