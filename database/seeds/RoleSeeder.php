<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Sgr\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now();

        Role::insert([
            [
                'role' => 'Admin',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'role' => 'Manager',
                'created_at' => $now,
                'updated_at' => $now
            ],
            [
                'role' => 'Operator',
                'created_at' => $now,
                'updated_at' => $now
            ]
        ]);
    }
}
