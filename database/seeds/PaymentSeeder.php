<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Sgr\Models\Payment;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $data = [];
        $payment = [Payment::PAYMENT_FAILED, Payment::PAYMENT_PENDING, Payment::PAYMENT_SUCCESSFUL];

        for($i = 10 ; $i < 130 ; $i++){
            array_push($data, [
                'booking_id' => $i,
                'payment_channel' => 'MPESA',
                'amount' => mt_rand(700, 1400),
                'system_transaction_id' => mt_rand(1,50),
                'provider_transaction_id' => 'DEMOFORNOW',
                'payment_status' => $payment[mt_rand(0,2)],
                'updatedAt'  => $faker->dateTime(),
                'createdAt' => $faker->dateTime()]);
        }

        Payment::insert($data);
    }
}
