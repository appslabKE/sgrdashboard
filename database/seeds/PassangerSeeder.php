<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Sgr\Models\Passanger;

class PassangerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $fullfillment = [Passanger::FULLFILMENT_ARRIVED, Passanger::FULLFILMENT_BOOKED,
            Passanger::FULLFILMENT_ONTRANSIT, Passanger::FULLFILMENT_PLACED];

        $ticket_type = ['child','adult'];

        $data = [];

        foreach (range(10,100) as $item){
            array_push($data,[
                'booking_id' => mt_rand(10,120),
                'class_id' => mt_rand(1,3),
                'name' => $faker->name,
                'id_number' => $faker->bankAccountNumber,
                'seat_no' => mt_rand(1,44),
                'ticket_no' => $faker->creditCardNumber,
                'ticket_type' => $ticket_type[mt_rand(0,1)],
                'amount_paid' => mt_rand(700,1400),
                'fulfillment_status' => $fullfillment[mt_rand(0,3)],
                'ticket_status' => mt_rand(0,4),
                'createdAt' => $faker->dateTime(),
                'updatedAt' => $faker->dateTime(),
            ]);
        }

        Passanger::insert($data);
    }
}
