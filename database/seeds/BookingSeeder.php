<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Sgr\Models\Booking;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $data = [];

        foreach (range(10,160) as $item){
            array_push($data,[
                'route_id' => mt_rand(1,3),
                'trade_id' => strtoupper(str_random(4,6)),
//                'train_id' => mt_rand(1,2),
                'booking_channel' => mt_rand(0,1),
                'date_of_travel' => $faker->date(),
                'total_passengers' => mt_rand(1,6),
                'total_children' => mt_rand(1,4),
                'phone' => $faker->phoneNumber,
                'departure_time' => $faker->time(),
                'arrival_time' => $faker->time(),
                'total_amount' => mt_rand(700,1400),
                'refunded_amount' => mt_rand(700,1400),
                'booking_status' => mt_rand(0,1),
                'createdAt' => $faker->dateTime(),
                'updatedAt' => $faker->dateTime()
            ]);
        }

        Booking::insert($data);
    }
}
