<?php

use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Super Admin',
            'phone' => '0704407117',
            'role_id' => 1,
            'email' => 'admin@sgr.com',
            'password' => bcrypt('Qwerty123!')
        ]);
    }
}
