<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Sgr\helpers\DataCache;
use Sgr\Repo\MetricsRepo;

class ChacheAllmetrics extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:allmetrics';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to cache all metrics';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        (new MetricsRepo())->allMetrics();
        $this->info('all metrics cached');
    }
}
