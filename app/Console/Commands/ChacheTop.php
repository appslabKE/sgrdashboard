<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Sgr\helpers\DataCache;
use Sgr\Repo\InsightsRepo;

class ChacheTop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:top';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache the top insights';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!Cache::has('topmisc')){

            $all_top = (new InsightsRepo())->misc();

            DataCache::putCacheData('topmisc', $all_top, 1440);

            $this->info('Top insight cached');

        }
    }
}
