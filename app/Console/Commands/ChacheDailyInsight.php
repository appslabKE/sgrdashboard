<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Sgr\helpers\DataCache;

class ChacheDailyInsight extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:groupby';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $groupby = ['%Y-%m-%d','%Y-%v','%Y-%M','%Y'];

        foreach ($groupby as $group)
        {
            if (! Cache::has($group)){

                $raw_statement = "DATE_FORMAT(bookings.created_at,'" . $group ."') as created_at,bookings.total_amount,
            bookings.booking_channel,passengers.class,payments.payment_status,payments.amount as payment_amount,
                passengers.ticket_status";

                $result = DB::table('bookings')
                    ->join('passengers', 'bookings.booking_id', '=', 'passengers.booking_id')
                    ->join('payments','bookings.booking_id','=','payments.booking_id')
                    ->where('bookings.created_at','>=', Carbon::now()->startOfMonth())
                    ->selectRaw($raw_statement)->get();

                DataCache::putCacheData($group , $result, 300);

                $this->info('cached group insights');


            }
        }

    }
}
