<?php

namespace App\Console\Commands;

use App\Mail\SmsNotification;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Sgr\helpers\GeneralHelper;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send sms balance notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

//        foreach ([
//                     "momido@krc.co.ke",
//                     "imwadime@krc.co.ke",
//                     "cmuriuki1@safaricom.co.ke",
//                     "sgr@buupass.com",
//                     "marvincollins14@gmail.com",
//                     "sonia@buupass.com",
//                     "leslie@buupass.com"
//                 ] as $account){

            $client =  new Client();
            $response = $client->get('http://pay.kenyasgr.com/balance?pass=secretKenyaSGR');

            $sms_count = json_decode($response->getBody())[0]->UnitsBalance;

            if ($sms_count < 10000) {

                $msg = "The remaining SMS in KenyaSGR system is " . $sms_count . "\n 
                visit http://dashboard.kenyasgr.com to see the remaining sms \n
                ***Do not reply to this email \n
                Thanks, \n
                KenyaSGR";

                GeneralHelper::sendMail('momido@krc.co.ke', 'smsbalance@kenyasgr.com',
                    $msg, 'SMS Balance ~ Kenya SGR', 'mtias?as?');

                GeneralHelper::sendMail('imwadime@krc.co.ke', 'smsbalance@kenyasgr.com',
                    $msg, 'SMS Balance ~ Kenya SGR', 'mtias?as?');

                GeneralHelper::sendMail('cmuriuki1@safaricom.co.ke', 'smsbalance@kenyasgr.com',
                    $msg, 'SMS Balance ~ Kenya SGR', 'mtias?as?');

                GeneralHelper::sendMail('marvin@appslab.co.ke', 'smsbalance@kenyasgr.com',
                    $msg, 'SMS Balance ~ Kenya SGR', 'mtias?as?');

                GeneralHelper::sendMail('sgr@buupass.com', 'smsbalance@kenyasgr.com',
                    $msg, 'SMS Balance ~ Kenya SGR', 'mtias?as?');
                GeneralHelper::sendMail('sonia@buupass.com', 'smsbalance@kenyasgr.com',
                    $msg, 'SMS Balance ~ Kenya SGR', 'mtias?as?');
                GeneralHelper::sendMail('leslie@buupass.com', 'smsbalance@kenyasgr.com',
                    $msg, 'SMS Balance ~ Kenya SGR', 'mtias?as?');
            }

//            Mail::to('marvin@appslab.co.ke')->send(new SmsNotification($sms_count));
//            }

        $this->info('Cron for sending email');

    }
}
