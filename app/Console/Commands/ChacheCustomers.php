<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Sgr\helpers\DataCache;

class ChacheCustomers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:customers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'storing customers in the cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (Cache::has('customers')){
            $customersRecords = DB::table('bookings')
                ->join('passengers', 'bookings.booking_id', '=', 'passengers.booking_id')
                ->join('payments', 'bookings.booking_id', '=', 'payments.booking_id')
                ->where('bookings.created_at','>=', Carbon::now()->startOfMonth())
                ->selectRaw("bookings.created_at,bookings.total_amount,passengers.name,passengers.id_number,
                 passengers.ticket_no,bookings.phone, bookings.source, bookings.destination,
                 bookings.date_of_travel,payments.amount as payment_amount")->get();

            DataCache::putCacheData('customers', $customersRecords, 1440);

            $this->info('Added customers to cache');
        }
    }
}
