<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Sgr\helpers\DataCache;

class ChacheAllInsight extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:allinsight';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command cache all insights';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (Cache::has('allInsights')){

            $query = DB::table('bookings')
                ->join('passengers', 'bookings.booking_id', '=', 'passengers.booking_id')
                ->join('payments','bookings.booking_id','=','payments.booking_id')
                ->where('bookings.created_at','>=', Carbon::now()->subMonth(2)->startOfMonth())
                ->selectRaw("DATE_FORMAT(bookings.created_at, '%Y-%m-%d') as created_at,bookings.total_amount,
            bookings.booking_channel,passengers.class,payments.payment_status,payments.amount as payment_amount,
                passengers.ticket_status")->get();

            DataCache::putCacheData('allInsights', $query, 300);

            $this->info('All Insights cached successfully');

        }

    }
}
