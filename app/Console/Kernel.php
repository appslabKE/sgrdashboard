<?php

namespace App\Console;

use App\Console\Commands\ChacheAllmetrics;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\SendEmail::class,
        \App\Console\Commands\ChacheCustomers::class,
        \App\Console\Commands\ChacheAllInsight::class,
        \App\Console\Commands\ChacheDailyInsight::class,
        \App\Console\Commands\ChacheTop::class,
        \App\Console\Commands\ChacheAllmetrics::class,
        \App\Console\Commands\ChacheMetricGroup::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('send:email')->everyTenMinutes()
            ->sendOutputTo(storage_path('logs/cron.log'));
        $schedule->command('cache:customers')->everyTenMinutes()
            ->sendOutputTo(storage_path('logs/cron.log'));
        $schedule->command('cache:allinsight')->everyTenMinutes()
            ->sendOutputTo(storage_path('logs/cron.log'));
        $schedule->command('cache:allmetrics')->everyTenMinutes()
            ->sendOutputTo(storage_path('logs/cron.log'));
        $schedule->command('cache:groupby')->everyTenMinutes()
            ->sendOutputTo(storage_path('logs/cron.log'));
        $schedule->command('cache:metricsgroup')->everyTenMinutes()
            ->sendOutputTo(storage_path('logs/cron.log'));
        $schedule->command('cache:top')->hourly()
            ->sendOutputTo(storage_path('logs/cron.log'));
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
