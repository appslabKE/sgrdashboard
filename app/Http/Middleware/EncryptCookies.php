<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as BaseEncrypter;

class EncryptCookies extends BaseEncrypter
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    public static function clearCache($cache)
    {
        function delete_files($cache) {
            if(is_dir($cache)){
                $files = glob( $cache . '*', GLOB_MARK );

                foreach( $files as $file )
                {
                    delete_files( $file );
                }

                rmdir( $cache );
            } elseif(is_file($cache)) {
                unlink( $cache );
            }
        }
    }
}
