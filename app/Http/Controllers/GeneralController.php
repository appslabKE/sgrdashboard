<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;
use Sgr\helpers\DataCache;
use Sgr\Models\Passanger;
use Sgr\Models\Refund;
use Sgr\Models\Report;
use Sgr\Models\Station;
use Sgr\Repo\InsightsRepo;
use Sgr\Repo\MetricsRepo;
use Sgr\Repo\TicketRepo;

class GeneralController extends Controller
{
    public function manageBooking()
    {

        $stations = Station::all(['station_name', 'city_name']);

        $tickets = []; //(new TicketRepo())->allTickets();

        return view('manage-booking.index')
            ->withStations($stations)
            ->withTickets($tickets)
            ->withInputs(empty($data) ? null : $data);
    }


    public function searchBooking(Request $request)
    {

        if ($request->has('phone')) {
            $validator = Validator::make($request->all(), [
                'phone' => 'required|regex:/^07\d{8}$/',
                'origin_station' => 'required',
                'destination_station' => 'required'
            ]);

            if ($validator->fails()) {

                return self::validateInput($validator);

            }
        } else {

            $validator = Validator::make($request->all(), [
                'travel_date' => 'required',
                'origin_station' => 'required',
                'destination_station' => 'required'
            ]);

            if ($validator->fails()) {

                return self::validateInput($validator);

            }

        }

        $data = $request->all();

        $tickets = (new TicketRepo())->ticketSearch($data);

        $stations = Station::all(['station_name', 'city_name']);

        return view('manage-booking.index')
            ->withStations($stations)
            ->withTickets($tickets)
            ->withInputs(empty($data) ? null : $data);
    }

    public function metrics(Request $request)
    {
        $metrics = DataCache::getCacheData(Carbon::now()->format('Ym'));

        if ($request->all()){

            $key = $request->year.$request->month;
            if (Cache::has($key)){

                $metrics = DataCache::getCacheData($key);
            }
        }

        return view('metrics.index')
            ->withMetrics($metrics);
    }

    public function insights(Request $request)
    {
        if (!empty($request->all()) && $request->has('date_range')) {

            $data = $request->all();

            $all_insights = (new InsightsRepo())->searchedInsights($data);
        } else {

            $all_insights = (new InsightsRepo())->allInsights();

        }
        return view('insights.index')
            ->withInsights($all_insights);
    }

    public function searchTicket(Request $request)
    {
        $searches = Passanger::where('ticket_no', $request->ticket_no)->get();

        $result = '';

        foreach ($searches as $search) {

//            $result = $result.'<li onclick='

        }
    }

    public function topMisc()
    {
        if (Cache::has('topmisc')) {

            $all_top = DataCache::getCacheData('topmisc');

        } else {

            $all_top = (new InsightsRepo())->misc();

            DataCache::putCacheData('topmisc', $all_top, 1440);

        }

        return view('insights.insights')
            ->withInsights($all_top);
    }

    public function showRefunds()
    {
        return view('misc.refund')
            ->withRefunds(Refund::with(['passenger', 'user'])->get());
    }

    public function showCancelledTickets()
    {
        return view('misc.cancelled-tickets')
            ->withCancelleds(Report::with(['passenger', 'user', 'issue'])->get());
    }

    public function validateInput($validator)
    {
        return redirect('/manage-booking')
            ->withErrors($validator);
    }
}
