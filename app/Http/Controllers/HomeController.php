<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PDO;
use Sgr\helpers\DataCache;
use Sgr\helpers\GeneralHelper;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = [];
        $total_payment = 0;
        $mpesa_payment = 0;
        $unconfirmed_bookings = 0;
        $total_tickets = 0;

        if (Cache::has('todays_booking')){

            $data = DataCache::getCacheData('todays_booking');

        }
        else{

            $query1 = DB::table('bookings')->whereDate('booking_date', \Carbon\Carbon::now()->subDay(1)->toDateString())
                ->join('passengers', 'bookings.booking_id', '=', 'passengers.booking_id')
                ->join('payments','bookings.booking_id','=','payments.booking_id')
                ->select('bookings.trade_id','bookings.booking_date','bookings.created_at',
                    'bookings.booking_channel','bookings.date_of_travel',
                    'bookings.total_passengers','bookings.total_children','bookings.phone',
                    'bookings.source','bookings.destination','bookings.booking_status',
                    'bookings.total_amount as booking_amount',
                    'payments.booking_id','payments.payment_status','payments.payment_channel',
                    'payments.amount as payment_amount',
                    'payments.provider_transaction_id','passengers.booking_id','passengers.name',
                    'passengers.ticket_no','passengers.booking_id as booking_number',
                    'passengers.id','passengers.ticket_status','passengers.amount_paid')
                ->where('payments.payment_status','paid');

            $statement = $query1->getConnection()->getPdo()->prepare($query1->toSql());
            $statement->execute($query1->getBindings());

            while ($record = $statement->fetch(PDO::FETCH_ASSOC)) {
                array_push($data,$record);
            }

            DataCache::putCacheData('todays_booking', $data, 3000);

        }


        $total_booking = count($data);

        $total_passengers = collect($data)->sum('total_passengers');
        $total_children = collect($data)->sum('total_children');

        foreach ($data as $datum){

            $total_payment = $total_payment + $datum['payment_amount'];
            $unconfirmed_bookings = $unconfirmed_bookings + $datum['payment_status'] == 'pending' ? 1 : 0;
            $total_tickets = $total_tickets + $datum['ticket_status'] == 'booked' ? 1 : 0;

            if ($datum['payment_channel'] == 'mpesa'){

                $mpesa_payment = $mpesa_payment + $datum['payment_amount'];

            }

        }

        return view('dashboard.index')
            ->withSms(GeneralHelper::getSmsData())
            ->withRecords([
                'total_booking' => $total_booking,
                'total_web_booking' => 0,
                'total_passengers' => $total_passengers,
                'total_children' => $total_children,
                'total_payment' => $total_payment,
                'total_tickets' => $total_tickets,
                'total_bookings' => $total_booking,
                'total_mpesa_payment' => $mpesa_payment,
                'total_unconfirmed_booking' => $unconfirmed_bookings,
                'total_web_unconfirmed_booking' => 0,
            ])
            ->withStatistics(collect($data)->sortByDesc('created_at')->take(50));
    }
}
