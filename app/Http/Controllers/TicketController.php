<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sgr\Models\Passanger;

class TicketController extends Controller
{
    public function show($id)
    {
        $ticket = Passanger::with(['booking.payment','booking'])->has('booking')->where('id',$id)->first();
//        dd($ticket);
        return view('tickets.show')
            ->withTicket($ticket);
    }
}
