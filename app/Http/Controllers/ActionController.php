<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Riazxrazor\LaravelSweetAlert\LaravelSweetAlert;
use Sgr\helpers\AThelper;
use Sgr\Models\Feedback;
use Sgr\Models\Issue;
use Sgr\Models\Passanger;
use Sgr\Models\Payment;
use Sgr\Models\Refund;
use Sgr\Models\Report;

class ActionController extends Controller
{
    public function resendSms($id)
    {
        $ticket = Passanger::with(['booking'])->where('id', $id)->first();
        return view('actions.resend-sms')
            ->withUser($ticket);
    }

    public function smsSave(Request $request)
    {
        (new AThelper())->getMessageDetails($request->phone_number, $request->message);

        return redirect('/manage-booking');
    }

    //done refund view
    public function refund($id)
    {
        $ticket = Passanger::with(['booking.payment'])->where('id', $id)->first();

        if ($ticket->ticket_status == Passanger::TICKET_CANCELED_NO_PAY ||
            $ticket->ticket_status == Passanger::TICKET_PLACED ||
            $ticket->ticket_status == Passanger::TICKET_REFUNDED){

            LaravelSweetAlert::setMessageError("You cannot refund this ticket");

            return redirect()->back();
        }

        return view('actions.refund')
            ->withUser($ticket);
    }

//done confirm page
    public function confirmPayment($id)
    {
        $ticket = Passanger::with(['booking.payment','refund'])->where('id', $id)->first();

        return view('actions.confirm-payment')
            ->withUser($ticket);
    }

// refund done
    public function refundSave(Request $request)
    {

        $user = Auth::user()->id;
        $ticket = Passanger::with(['booking'])->where('id', $request->ticket_id)->first();

        if ((int) $request->amount < 1 || $request->amount == null){

            LaravelSweetAlert::setMessageError('The refund amount cannot be ZERO');
        }

        elseif  ($ticket->ticket_status == Passanger::TICKET_CONFIRMED || $ticket->ticket_status == Passanger::TICKET_CANCELED_PAID){

            self::createRefund([
                'user_id' => $user,
                'passenger_id' => $request->ticket_id,
                'refund_method'  =>$request->refund_method,
                'refund_amount' => $request->amount,
                'reason' => $request->message,
                'status' => Refund::PENDING
                ]
            );
        }

        else{

            LaravelSweetAlert::setMessageError("Refund request was unsuccessful");
            return redirect('/manage-booking');

        }

        LaravelSweetAlert::setMessageSuccess("Refund Request Successful");
        return redirect('/manage-booking');

    }

    public function feedback($id)
    {
        $ticket = Passanger::with(['booking','refund'])->where('id', $id)->first();
        return view('actions.feedback')
            ->withUser($ticket);
    }

//done cancel
    public function cancelTicket($id)
    {
        $ticket = Passanger::with(['booking'])->where('id', $id)->first();

        if ($ticket->ticket_status == Passanger::TICKET_CANCELED_NO_PAY ||
            $ticket->ticket_status == Passanger::TICKET_CANCELED_PAID ||
            $ticket->ticket_status == Passanger::TICKET_CONFIRMED ||
            $ticket->ticket_status == Passanger::TICKET_REFUNDED){

            LaravelSweetAlert::setMessageError("You cannot cancel this ticket");

            return redirect()->back();
        }

        return view('actions.cancel-ticket')
            ->withIssues(Issue::all())
            ->withTicket($ticket);
    }

    public function confirmPaymentSave(Request $request)
    {

        $user = Auth::user()->id;

        $payment = Payment::findorfail($request->payment_id);

        if (str_replace(" ",'',$payment->payment_status) == $request->payment_status){

            LaravelSweetAlert::setMessageError("Status change unsuccessful");

            return redirect('/manage-booking');

        }

        DB::update("update payments set payment_status = '".$request->payment_status."' where payment_id = ".$request->payment_id);

        self::createReport([
            'user_id' => $user,
            'passenger_id' => $request->ticket_id,
            'issue_id' => 1,
            'message' => 'Payment status changed by '. Auth::user()->name,
            'action' => 'Payment Status Changed Mpesa Ref is '. $request->mpesa_ref]);

        LaravelSweetAlert::setMessageSuccess("Payment Status Changed Successfully");
        return redirect('/manage-booking');
    }

    public function cancelTicketSave(Request $request)
    {
        $user = Auth::user()->id;

        $ticket = Passanger::where('id', $request->ticket_id)->first();

        if ($ticket->ticket_status == Passanger::TICKET_PLACED){

            self::updateTicket($request->ticket_id,Passanger::TICKET_CANCELED_NO_PAY);

            self::createReport([
                'user_id' => $user,
                'passenger_id' => $request->ticket_id,
                'issue_id' => $request->issue_id,
                'message' => $request->message,
                'action' => 'Ticket Canceled without payment']);
        }

        elseif  ($ticket->ticket_status == Passanger::TICKET_CONFIRMED){

            self::updateTicket($request->ticket_id,Passanger::TICKET_CANCELED_PAID);

            self::createReport([
                'user_id' => $user,
                'passenger_id' => $request->ticket_id,
                'issue_id' => $request->issue_id,
                'message' => $request->message,
                'action' => 'Ticket Canceled with payment']);
        }

        else{
            LaravelSweetAlert::setMessageError("Ticket Not Canceled");
            return redirect('/manage-booking');

        }

        LaravelSweetAlert::setMessageSuccess("Ticket Canceled Successful");
        return redirect('/manage-booking');
    }

    public function feedbacksave(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;

        Feedback::create($data);

        LaravelSweetAlert::setMessageSuccess("Feedback Added Successful");

        return redirect('/manage-booking');
    }

    public function createReport($data)
    {
        Report::create($data);
    }

    public function updateTicket($id, $status)
    {
        DB::update("update passengers set ticket_status = '".$status."'  where id =".$id);

    }

    public function refundCustomer(Request $request)
    {
        $id = $request->id;

        $refund = Refund::findorfail($request->refund);


        $ticket = Passanger::with(['booking'])->where('id', $id)->first();

        if  ($refund->status == Refund::PENDING || $ticket->ticket_status == Passanger::TICKET_CONFIRMED || $ticket->ticket_status == Passanger::TICKET_CANCELED_PAID){

            $refund->status = Refund::CONFIRMED;

            $refund->save();

            self::updateTicket($id,Passanger::TICKET_REFUNDED);

            LaravelSweetAlert::setMessageSuccess("User refund confirmed ");

            return redirect()->back();
        }

        LaravelSweetAlert::setMessageError('The refund UNSUCCESSFUL');

        return redirect()->back();
    }

    public function createRefund($data)
    {
        Refund::create($data);
    }
}
