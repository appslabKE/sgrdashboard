<?php

namespace App\Http\Controllers;

use App\Report;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Excel;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Riazxrazor\LaravelSweetAlert\LaravelSweetAlert;
use Sgr\helpers\DataCache;
use Sgr\helpers\GeneralHelper;
use Sgr\Models\Issue;
use Sgr\Repo\Collection;
use Sgr\Repo\ReportRepo;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('issue-report.create')
            ->withIssuetypes(Issue::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        //
    }

    public function customers(Request $request)
    {
        $customers = '';

        if ($request->all() != null){

            $customers  = DB::table('bookings')
                ->join('passengers', 'bookings.booking_id', '=' ,'passengers.booking_id')
                ->join('payments', 'payments.booking_id', '=', 'bookings.booking_id')
                ->whereBetween('bookings.booking_id', [$request->previous,$request->next])
                ->selectRaw('bookings.booking_id, bookings.created_at, bookings.total_amount, passengers.name, passengers.id_number, payments.amount,
                 passengers.ticket_no, bookings.phone, bookings.source, bookings.destination,
                 bookings.date_of_travel,payments.amount as payment_amount')->orderBy('bookings.created_at', 'desc')->take(25)->get();

        }
        else{
            $count = DB::select('select count(booking_id) as count from bookings')[0]->count;
            $customers  = DB::table('bookings')
                ->join('passengers', 'bookings.booking_id', '=' ,'passengers.booking_id')
                ->join('payments', 'payments.booking_id', '=', 'bookings.booking_id')
                ->whereDate('bookings.created_at', Carbon::now()->subDay(1)->toDateString())
                ->where('bookings.booking_id', '>=', ($count - 28))
                ->selectRaw('bookings.booking_id, bookings.created_at, bookings.total_amount, passengers.name, passengers.id_number, payments.amount,
                 passengers.ticket_no, bookings.phone, bookings.source, bookings.destination,
                 bookings.date_of_travel,payments.amount as payment_amount')
                ->orderBy('bookings.created_at', 'desc')->take(25)->get();
        }


        return view('reports.customers')
            ->withCustomers($customers);

    }

    public function downloadExcel(Request $request)
    {
        $dates = GeneralHelper::separateDates($request->all());

        $numberOfDays = $dates['start_date']->diffInDays($dates['ending_date']);

        if ($numberOfDays > 7){

            LaravelSweetAlert::setMessageError('Select 7 days date range');
            return redirect()->back();
        }

        ReportRepo::downloadExcel($request->all());
        return redirect()->back();
    }
}
