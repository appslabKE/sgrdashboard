<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SmsNotification extends Mailable
{
    use Queueable, SerializesModels;

    public $sms_count;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($sms_count)
    {
        $this->sms_count = $sms_count;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.sms.notification')
            ->subject('SMS Balance Notification - KenyaSGR');
    }
}
