let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
    'public/global/plugins/jquery.min.js',
    'public/global/plugins/bootstrap/js/bootstrap.min.js',
    'public/global/plugins/js.cookie.min.js',
    'public/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
    'public/global/plugins/jquery.blockui.min.js',
    'public/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
    'public/global/plugins/moment.min.js',
    'public/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js',
    'public/global/plugins/morris/morris.min.js',
    'public/global/plugins/morris/raphael-min.js',
    'public/global/plugins/counterup/jquery.waypoints.min.js',
    'public/global/plugins/counterup/jquery.counterup.min.js',
    'public/global/plugins/amcharts/amcharts/amcharts.js',
    'public/global/plugins/amcharts/amcharts/serial.js',
    'public/global/plugins/amcharts/amcharts/pie.js',
    'public/global/plugins/amcharts/amcharts/radar.js',
    'public/global/plugins/amcharts/amcharts/themes/light.js',
    'public/global/plugins/amcharts/amcharts/themes/patterns.js',
    'public/global/plugins/amcharts/amcharts/themes/chalk.js',
    'public/global/plugins/amcharts/ammap/ammap.js',
    'public/global/plugins/amcharts/ammap/maps/js/worldLow.js',
    'public/global/plugins/amcharts/amstockcharts/amstock.js',
    'public/global/plugins/fullcalendar/fullcalendar.min.js',
    'public/global/plugins/horizontal-timeline/horozontal-timeline.min.js',
    'public/global/plugins/flot/jquery.flot.min.js',
    'public/global/plugins/flot/jquery.flot.resize.min.js',
    'public/global/plugins/flot/jquery.flot.categories.min.js',
    'public/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js',
    'public/global/plugins/jquery.sparkline.min.js',
    'public/global/scripts/app.min.js',
    'public/pages/scripts/dashboard.min.js',
    'public/layouts/layout2/scripts/layout.min.js',
    'public/js/datepicker.js',
    'public/js/datepickerrange.js',
    'public/layouts/layout2/scripts/demo.min.js'], 'public/js/allsgr.js')
    .styles([
        'public/global/plugins/bootstrap/css/bootstrap.min.css',
        'public/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        'public/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css',
        'public/global/plugins/morris/morris.css',
        'public/global/plugins/fullcalendar/fullcalendar.min.css',
        'public/global/css/components.min.css',
        'public/global/css/plugins.min.css',
        'public/layouts/layout2/css/layout.min.css',
        'public/layouts/layout2/css/themes/blue.min.css',
        'public/layouts/layout2/css/custom.min.css',
        'public/pages/css/search.min.css',
        'public/css/datepicker.css',
        'public/css/datepickerrange.css',
        ], 'public/css/allsgr.css')
    .options({
        processCssUrls: false
    });
