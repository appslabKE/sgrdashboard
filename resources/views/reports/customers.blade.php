@extends('layouts.main')

@section('css')

@endsection
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">

            @component('components.breadcump')

                @slot('title')
                    Customer Report
                @endslot

                @slot('span')
                        generates insights on your customers
                @endslot

                reports
                @slot('menu')
                @endslot
            @endcomponent

            <div class="search-page search-content-1">
                <div class="row">
                    <div class="col-sm-4 pull-right">
                        <form action="{{ url('reports/download-excel') }}" method="get">
                            <div class="row">
                                {{--<div class="col-sm-4 form-group">--}}
                                    {{--<label>Group By</label>--}}
                                    {{--<div class="form-group">--}}
                                        {{--<select name="groupby" id="groupby" class="form-control">--}}
                                            {{--<option value="%Y-%m-%d">Daily</option>--}}
                                            {{--<option value="%Y-%v">Weekly</option>--}}
                                            {{--<option value="%Y-%M">Monthly</option>--}}
                                            {{--<option value="%Y">Yearly</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="col-sm-12 form-group">
                                    <label>Select Date Range</label>
                                    <div class="input-group">
                                        <input name="date_range" id="date_range" type="text" required placeholder="Date Range" value="" class="datepicker form-control date-picker">
                                        <span class="input-group-btn">
                                        <button class="btn blue uppercase bold" type="submit">Export To Excel</button>
                                    </span>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase">Customer Report</span>
                                </div>
                                <div class="tools">
                                    <a href="{{ url('reports/customers/?previous='.($customers->last()->booking_id - 25) .'&next='.$customers->last()->booking_id) }}"><strong><i class="fa fa-angle-left fa-3x"></i></strong></a>
                                    <a href="{{ url('reports/customers/?previous='.$customers->first()->booking_id .'&next='.($customers->first()->booking_id + 25)) }}"><strong><i class="fa fa-angle-right fa-3x"></i></strong></a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                                    <thead>
                                    <tr>
                                        <th >Full Name</th>
                                        <th>Phone</th>
                                        <th>National ID </th>
                                        <th>Origin </th>
                                        <th>Destination</th>
                                        <th>Date & Time of travel</th>
                                        <th class="text-right">Fare</th>
                                        <th>Hops</th>
                                        <th>Ticket Number</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($customers as  $customer)
                                        <tr>
                                            <td>{{ $customer->name }}</td>
                                            <td>{{ $customer->phone }}</td>
                                            <td>{{ $customer->id_number }}</td>
                                            <td>{{ $customer->source }}</td>
                                            <td>{{ $customer->destination }}</td>
                                            <td>{{ \Carbon\Carbon::parse($customer->date_of_travel) }}</td>
                                            <td class="text-right">{{ number_format($customer->payment_amount, 2) }}</td>
                                            <td></td>
                                            <td>{{ $customer->ticket_no }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th >Full Name</th>
                                            <th>Phone</th>
                                            <th>National ID </th>
                                            <th>Origin </th>
                                            <th>Destination</th>
                                            <th>Date of travel</th>
                                            <th class="text-right">Fare</th>
                                            <th>Hops</th>
                                            <th>Tickets</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

