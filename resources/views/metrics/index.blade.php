@extends('layouts.main')

@section('css')

@endsection
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">

            @component('components.breadcump')

                @slot('title')
                    Metrics
                @endslot

                @slot('span')
                    manage metrics
                @endslot

                metrics
                    @slot('menu')
                        {{--<div class="page-toolbar">--}}
                            {{--<div class="btn-group pull-right">--}}
                                {{--<a href="{{ route('roles.create') }}" class="btn btn-fit-height grey-salt" > Add Role--}}
                                    {{--<i class="fa fa-angle-down"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    @endslot
            @endcomponent

            <div class="search-page search-content-1">
                <div class="row ">
                    <div class="col-sm-4">
                        <form action="{{ url('metrics') }}" method="get">
                            <div class="row">
                                <div class="col-sm-4 form-group">
                                    <label>Group By</label>
                                    <div class="form-group">
                                        <select name="year" id="year" class="form-control">
                                            <option value="">Select Year</option>
                                            <option value="{{ \Carbon\Carbon::now()->year }}">{{ \Carbon\Carbon::now()->year }}</option>
                                            @for($i = 1 ; $i < 3; $i++)
                                                <option value="{{ \Carbon\Carbon::now()->subYears($i)->year }}">{{ \Carbon\Carbon::now()->subYears($i)->year }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-8 form-group">
                                    <label>Select Date Range</label>
                                    <div class="input-group">
                                        <select name="month" required id="month" class="form-control">
                                            <option value="">Select month</option>
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                        <span class="input-group-btn">
                                        <button class="btn blue uppercase bold" type="submit">Show</button>
                                    </span>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase">Tickets Metrics</span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <div class="portlet-body">
                                <table id="bookingtable" class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                                    <thead>
                                    <tr>
                                        <th class="text-center" >Date</th>
                                        <th class="text-center">Pending</th>
                                        <th class="text-center">Paid </th>
                                        <th class="text-center">Canceled </th>
                                        <th class="text-center">Pending Amount</th>
                                        <th class="text-center">Paid Amount</th>
                                        <th class="text-center">Canceled Amount</th>
                                        {{--<th class="text-right">Refunded Amount</th>--}}
                                        <th class="text-right">Net Amount</th>
                                        {{--<th class="text-center">Action</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($metrics as $key => $metric)
                                            <tr>
                                                <td class="text-center">{{ $metric['date'] }}</td>
                                                <td class="text-center">{{ $metric['reserved_tickets'] }}</td>
                                                <td class="text-center">{{ $metric['paid_tickets'] }}</td>
                                                <td class="text-center">{{ $metric['canceled_tickets'] }}</td>
                                                <td class="text-center">{{ number_format($metric['reserved_amount'],2 )}}</td>
                                                <td class="text-right">{{ number_format($metric['paid_amount'], 2) }}</td>
                                                <td class="text-right">{{ number_format($metric['canceled_amount'], 2) }}</td>
{{--                                                <td class="text-right">{{ number_format($metric['refunded_amount'], 2) }}</td>--}}
                                                <td class="text-right">{{ number_format(($metric['reserved_amount'] + $metric['paid_amount']) - $metric['canceled_amount'],2) }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{--{{ $metrics->links() }} --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

        @endsection

        @section('scripts')

@endsection

