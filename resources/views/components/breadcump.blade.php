<h1 class="page-title"> {{ $title }}
    <small>{{ $span }}</small>
</h1>
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="index.html">Home</a>
            <i class="fa fa-angle-right"></i>
        </li>

        <li>
            {{ $slot }}
        </li>

    </ul>
    {{ $menu }}
</div>