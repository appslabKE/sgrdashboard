<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>SGR</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="sgr client" />
    <meta content="" name="marvin" />
    @LaravelSweetAlertCSS
    <link href="{{ asset('css/allsgr.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/datatables.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" />


    @yield('css')

    <link rel="shortcut icon" href="favicon.ico" /> </head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">

    @include('partials.topnav')

<div class="page-container">

    @include('partials.sidenav')

    @yield('content')

{{--    @include('partials.quicksidebar')--}}

    @include('partials.footer')
</div>

    <!--[if lt IE 9]>
<script src="{{ asset('global/plugins/respond.min.js') }}"></script>
<script src="{{ asset('global/plugins/excanvas.min.js') }}"></script>
<script src="{{ asset('global/plugins/excanvas.min.js') }}"></script>
<![endif]-->
<script src="{{ asset('js/allsgr.js') }}" type="text/javascript"></script>
{{--<script src="{{ asset('js/bootstrap_datatables.js') }}" type="text/javascript"></script>--}}
<script src="{{ asset('js/datatables.js') }}" type="text/javascript"></script>

@yield('scripts')

    <script type="text/javascript">

        $(document).ready(function () {
            $('#bookingtable').DataTable();
        });

        $(function() {

            var start = moment().subtract(3, 'month');
            var end = moment();

            function cb(start, end) {
                $('#date_range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }

            $('#date_range').daterangepicker({
                startDate: start,
                endDate: end,
                ranges: {
//                    'Today': [moment(), moment()],
//                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
//                    'Year': [moment().subtract(12, 'month').startOf('month'), moment().endOf('month')]
                }
            }, cb);

            cb(start, end);

        });
    </script>
    @LaravelSweetAlertJS

</body>

</html>