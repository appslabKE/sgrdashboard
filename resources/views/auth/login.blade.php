@extends('layouts.auth.auth')

@section('content')

<div class="content">

       <form class="login-form" method="post" action="{{ route('login') }}">

           {{csrf_field()}}

           <h3 class="form-title">Login</h3>
           <div class="alert alert-danger display-hide">
               <button class="close" data-close="alert"></button>
               <span> Enter any username and password. </span>
           </div>
           <div class="form-group">
               <label class="control-label visible-ie8 visible-ie9">Phone Number</label>
               <div class="input-icon">
                   <i class="fa fa-user"></i>
                   <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Phone Number" name="phone" />
                   @if ($errors->has('phone'))
                       <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                   @endif
               </div>
           </div>
           <div class="form-group">
               <label class="control-label visible-ie8 visible-ie9">Password</label>
               <div class="input-icon">
                   <i class="fa fa-lock"></i>
                   <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />
                   @if ($errors->has('password'))
                       <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                   @endif
               </div>
           </div>
           <div class="form-actions">
               <label class="rememberme mt-checkbox mt-checkbox-outline">
                   <input type="checkbox" name="remember" value="1" /> Remember me
                   <span></span>
               </label>
               <button type="submit" class="btn green pull-right"> Login </button>
           </div>
           <p class="text-center">Makadara Express - Online Booking Dashboard <br>
               {{--All Right Reserved &copy; {{ \Carbon\Carbon::now()->year }} developed by <a target="_blank" href="https://appslab.co.ke/">Apps:Lab Co</a></p>--}}
       </form>


</div>

@endsection
