@extends('layouts.main')

@section('css')

@endsection
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">

            @component('components.breadcump')

                @slot('title')
                    Refund Requests
                @endslot

                @slot('span')

                @endslot

                refunds

                @slot('menu')

                @endslot

            @endcomponent

            <div class="search-page search-content-1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase">Refund Requests</span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <div class="portlet-body">
                                <table id="bookingtable" class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                                    <thead>
                                    <tr>
                                        <th >NO:</th>
                                        <th >Customer Name</th>
                                        <th >Refund Method</th>
                                        <th >Refund Amount</th>
                                        <th >Reason</th>
                                        <th >Requested By</th>
                                        <th >Status</th>
                                        <th >Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($refunds->sortByDesc('created_at') as $refund)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ ucwords($refund->passenger->name) }}</td>
                                            <td>{{ ucwords($refund->refund_method) }}</td>
                                            <td>KSH : {{ number_format($refund->refund_amount,2) }}</td>
                                            <td>{{ $refund->reason }}</td>
                                            <td>{{ $refund->user->name }}</td>
                                            <td>{{ $refund->status == \Sgr\Models\Refund::CONFIRMED ? 'Refunded' : 'Pending' }}</td>
                                            <td>
                                                @if($refund->status == \Sgr\Models\Refund::PENDING )
                                                    <form action="{{ url('actions/refund/confirm') }}" method="post">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="id" value="{{ $refund->passenger_id }}">
                                                        <input type="hidden" name="refund" value="{{ $refund->id }}">
                                                        <button class="btn btn-primary">Refund</button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfooter>
                                        <tr>
                                            <th >NO:</th>
                                            <th >Customer Name</th>
                                            <th >Refund Method</th>
                                            <th >Refund Amount</th>
                                            <th >Reason</th>
                                            <th >Requested By</th>
                                            <th >Status</th>
                                            <th >Action</th>
                                        </tr>
                                    </tfooter>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('scripts')

@endsection

