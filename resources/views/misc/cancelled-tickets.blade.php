@extends('layouts.main')

@section('css')

@endsection
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">

            @component('components.breadcump')

                @slot('title')
                    Cancelled Tickets
                @endslot

                @slot('span')
                    tickets
                @endslot

                cancelled tickets

                @slot('menu')

                @endslot

            @endcomponent

            <div class="search-page search-content-1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase">Cancelled Tickets</span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <div class="portlet-body">
                                <table id="bookingtable" class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                                    <thead>
                                    <tr>
                                        <th >NO:</th>
                                        <th >Customer Name</th>
                                        <th >Ticket Number</th>
                                        <th >Issue</th>
                                        <th >Reason</th>
                                        <th >User</th>
                                        <th >Action Taken</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($cancelleds->sortByDesc('created_at') as $cancelled)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ ucwords($cancelled->passenger->name) }}</td>
                                            <td>{{ ucwords($cancelled->passenger->ticket_no) }}</td>
                                            <td>{{ ucwords($cancelled->issue->issue) }}</td>
                                            <td>{{ ucfirst($cancelled->message) }}</td>
                                            <td>{{ ucfirst($cancelled->user->name) }}</td>
                                            <td>{{ ucfirst($cancelled->action) }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th >NO:</th>
                                            <th >Customer Name</th>
                                            <th >Ticket Number</th>
                                            <th >Issue</th>
                                            <th >Reason</th>
                                            <th >User</th>
                                            <th >Action Taken</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('scripts')

@endsection

