<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <div class="page-logo">
            <a href="{{ url('/') }}">
                <img src="{{ asset('images/logo.png') }}" alt="logo" class="logo-default" /> </a>
            <div class="menu-toggler sidebar-toggler">
            </div>
        </div>
        <a href="javascript:;" style="color: white" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <div class="page-actions">
            <div class="btn-group">
                {{--<button type="button" class="btn btn-circle btn-outline red dropdown-toggle" data-toggle="dropdown">--}}
                    {{--<i class="fa fa-plus"></i>&nbsp;--}}
                    {{--<span class="hidden-sm hidden-xs">New&nbsp;</span>&nbsp;--}}
                    {{--<i class="fa fa-angle-down"></i>--}}
                {{--</button>--}}
                {{--<ul class="dropdown-menu" role="menu">--}}
                    {{--<li>--}}
                        {{--<a href="javascript:;">--}}
                            {{--<i class="icon-docs"></i> New Post </a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="javascript:;">--}}
                            {{--<i class="icon-tag"></i> New Comment </a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="javascript:;">--}}
                            {{--<i class="icon-share"></i> Share </a>--}}
                    {{--</li>--}}
                    {{--<li class="divider"> </li>--}}
                    {{--<li>--}}
                        {{--<a href="javascript:;">--}}
                            {{--<i class="icon-flag"></i> Comments--}}
                            {{--<span class="badge badge-success">4</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li>--}}
                        {{--<a href="javascript:;">--}}
                            {{--<i class="icon-users"></i> Feedbacks--}}
                            {{--<span class="badge badge-danger">2</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            </div>
        </div>
        <div class="page-top">
            {{--<form class="search-form search-form-expanded" action="page_general_search_3.html" method="GET">--}}
                {{--<div class="input-group">--}}
                    {{--<input type="text" class="form-control" placeholder="Search ticket" name="query">--}}
                    {{--<span class="input-group-btn">--}}
                                {{--<a href="javascript:;" class="btn submit">--}}
                                    {{--<i class="icon-magnifier"></i>--}}
                                {{--</a>--}}
                            {{--</span>--}}
                {{--</div>--}}
            {{--</form>--}}
            <div class="top-menu">

                @if (! Auth::guest())
                    <ul class="nav navbar-nav pull-right">
                    {{--<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">--}}
                        {{--<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">--}}
                            {{--<i class="icon-bell"></i>--}}
                            {{--<span class="badge badge-default"> 7 </span>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu">--}}
                            {{--<li class="external">--}}
                                {{--<h3>--}}
                                    {{--<span class="bold">12 pending</span> notifications</h3>--}}
                                {{--<a href="page_user_profile_1.html">view all</a>--}}
                            {{--</li>--}}
                            {{--<li>--}}
                                {{--<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">--}}
                                    {{--<li>--}}
                                        {{--<a href="javascript:;">--}}
                                            {{--<span class="time">just now</span>--}}
                                            {{--<span class="details">--}}
                                                        {{--<span class="label label-sm label-icon label-success">--}}
                                                            {{--<i class="fa fa-plus"></i>--}}
                                                        {{--</span> New booking </span>--}}
                                        {{--</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}

                   <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="{{ asset('images/avatar.png') }}" />
                            <span class="username username-hide-on-mobile"> {{ ucwords(Auth::user()->name) }} </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            {{--<li>--}}
                                {{--<a href="#">--}}
                                    {{--<i class="icon-lock"></i> Lock Screen </a>--}}
                            {{--</li>--}}
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="icon-key"></i> Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
                @endif
            </div>
        </div>
    </div>
</div>

<div class="clearfix"> </div>