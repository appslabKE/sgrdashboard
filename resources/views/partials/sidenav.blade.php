<div class="page-sidebar-wrapper">
    <!-- END SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse"><ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="nav-item {{ Request::segment(1) == '' || Request::segment(1) == 'home' ? 'start active open' : '' }}">
                <a href="{{ url('') }}" class="nav-link nav-toggle">
                    <i class="icon-home menu-icon-color"></i>
                    <span class="title menu-icon-color">Dashboard</span>
                    @if(Request::segment(1) == '' || Request::segment(1) == 'home')
                    <span class="selected"></span>
                        <span class="arrow open"></span>
                    @endif

                </a>
            </li>
            <li class="nav-item {{ Request::segment(1) == 'manage-booking' ? 'start active open' : '' }} ">
                <a href="{{ url('/manage-booking') }}" class="nav-link nav-toggle">
                    <i class="icon-magnifier-add menu-icon-color"></i>
                    <span class="title menu-icon-color">Manage Bookings</span>
                    @if(Request::segment(1) == 'manage-booking')
                    <span class="selected"></span>
                    <span class="arrow open"></span>
                        @endif
                </a>

            </li>
            <li class="nav-item  {{ Request::segment(1) == 'metrics' ? 'start active open' : '' }}  ">
                <a href="{{ url('metrics') }}" class="nav-link nav-toggle">
                    <i class="icon-graph menu-icon-color"></i>
                    <span class="title menu-icon-color">Metrics</span>
                    @if(Request::segment(1) == 'metrics')
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @endif
                </a>
            </li>
            <li class="nav-item  {{ Request::segment(1) == 'insights' ? 'start active open' : '' }} ">
                <a href="{{ url('/insights') }}" class="nav-link nav-toggle">
                    <i class="icon-bar-chart menu-icon-color"></i>
                    <span class="title menu-icon-color">Insights</span>
                    @if(Request::segment(1) == 'insights')
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @endif
                </a>
            </li>
            <li class="nav-item  {{ Request::segment(1) == 'reports' ? 'start active open' : '' }} ">
                <a href="#" class="nav-link nav-toggle">
                    <i class="icon-docs menu-icon-color"></i>
                    <span class="title menu-icon-color">Reports</span>
                    @if(Request::segment(1) == 'reports')
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="{{ url('reports/customers') }}" class="nav-link ">
                            <span class="title menu-icon-color">Customers Report</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item   {{ Request::segment(1) == 'misc' ? 'start active open' : '' }}">
                <a href="" class="nav-link nav-toggle">
                    <i class="icon-speedometer menu-icon-color"></i>
                    <span class="title menu-icon-color">Misc</span>
                    @if(Request::segment(1) == 'misc')
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="{{ url('misc/tops') }}" class="nav-link ">
                            <span class="title menu-icon-color">Top Insights</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        {{--{{ url('misc/feedback') }}--}}
                        <a href="#" class="nav-link ">
                            <span class="title menu-icon-color">Feedback(coming soon)</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
{{--                        {{ url('misc/refunds') }}--}}
                        <a href="#" class="nav-link ">
                            <span class="title menu-icon-color">Refunds(coming soon)</span>
                        </a>
                    </li>

                    <li class="nav-item  ">
                        {{--{{ url('misc/cancelled-tickets') }}--}}
                        <a href="#" class="nav-link ">
                            <span class="title menu-icon-color">Updated Tickets(coming soon)</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="nav-item  {{ Request::segment(1) == 'settings' ? 'start active open' : '' }}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-settings menu-icon-color"></i>
                    <span class="title menu-icon-color">System Setting</span>
                    @if(Request::segment(1) == 'settings')
                        <span class="selected"></span>
                        <span class="arrow open"></span>
                    @endif
                </a>
                <ul class="sub-menu">
                    <li class="nav-item  ">
                        <a href="{{ route('users.index') }}" class="nav-link ">
                            <span class="title menu-icon-color">Users</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ route('issues.index') }}" class="nav-link ">
                            <span class="title menu-icon-color">Issues</span>
                        </a>
                    </li>
                    <li class="nav-item  ">
                        <a href="{{ route('roles.index') }}" class="nav-link ">
                            <span class="title menu-icon-color">Roles</span>
                        </a>
                    </li>

                </ul>
            </li>
        </ul>
    </div>
</div>