@extends('layouts.main')

@section('content')

    <div class="page-content-wrapper">
            <div class="page-content">

                @component('components.breadcump')

                    @slot('title')
                        Dashboard
                    @endslot

                    @slot('span')
                        dashboard and statistics
                    @endslot

                    dashboard

                    @slot('menu')
                    @endslot
                @endcomponent

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-red-haze">
                                        <span>{{ $records['total_passengers'] }}</span>
                                    </h3>
                                    <small>TICKETS SOLD TODAY</small>
                                </div>
                                <div class="icon">
                                    {{--<i class="icon-users"></i>--}}
                                </div>
                            </div>
                            <div class="progress-info">
                                <div>
                                    <h4>
                                        Adults : {{ ($records['total_passengers'] - $records['total_children'] )  }} | Children : {{ $records['total_children']  }}
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-blue-sharp">
                                        <span>{{ number_format($records['total_payment'], 0) }}</span>
                                    </h3>
                                    <small>CASH COLLECTED</small>
                                </div>
                                <div class="icon">
                                    {{--<i class="icon-money">$</i>--}}
                                </div>
                            </div>
                            <div class="progress-info">
                                <div>
                                    <h4>Mpesa : {{ number_format($records['total_mpesa_payment'], 0)  }} | Others : {{ number_format(($records['total_payment'] - $records['total_mpesa_payment'] ), 0) }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="dashboard-stat2 ">
                            <div class="display">
                                <div class="number">
                                    <h3 class="font-purple-soft">
                                        <span>{{ $sms['bal'] }}</span>
                                    </h3>
                                    <small>SMS BALANCE</small>
                                </div>
                                <div class="icon">
                                    {{--<i class="icon-bubble"></i>--}}
                                </div>
                            </div>
                            <div class="progress-info">
                                <div>
                                    <h4> Expiry Date : {{ $sms['expiry_date'] }}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption caption-md">
                                    <i class="icon-bar-chart font-dark hide"></i>
                                    <span class="caption-subject font-dark bold uppercase">Booking Statistics</span>
                                    <span class="caption-helper"></span>
                                </div>
                                <div class="actions">
                                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                                        <label class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm active">
                                            <input type="radio" name="options" class="toggle" id="option1">Today's Record</label>
                                        {{--<label class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">--}}
                                            {{--<input type="radio" name="options" class="toggle" id="option2">Week</label>--}}
                                        {{--<label class="btn btn-transparent blue-oleo btn-no-border btn-outline btn-circle btn-sm">--}}
                                            {{--<input type="radio" name="options" class="toggle" id="option2">Month</label>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="portlet-body">
                                {{--<div class="row number-stats margin-bottom-30">--}}
                                    {{--<div class="col-md-6 col-sm-6 col-xs-6">--}}
                                        {{--<div class="stat-left">--}}
                                            {{--<div class="stat-chart">--}}
                                                {{--<!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->--}}
                                                {{--<div id="sparkline_bar"></div>--}}
                                            {{--</div>--}}
                                            {{--<div class="stat-number">--}}
                                                {{--<div class="title"> <h4> Today Bookings</h4> </div>--}}
                                                {{--<div class="number"> {{ count($statistics) }} </div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-md-6 col-sm-6 col-xs-6">--}}
                                        {{--<div class="stat-right">--}}
                                            {{--<div class="stat-chart">--}}
                                                {{--<!-- do not line break "sparkline_bar" div. sparkline chart has an issue when the container div has line break -->--}}
                                                {{--<div id="sparkline_bar2"></div>--}}
                                            {{--</div>--}}
                                            {{--<div class="stat-number">--}}
                                                {{--<div class="title"> <h4>Today Payment</h4> </div>--}}
                                                {{--<div class="number"> {{ number_format($statistics->sum('payment_amount'), 2)  }} </div>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                <div class="table-scrollable table-scrollable-borderless">
                                    <table id="bookingtable" class="table display table-hover table-light">
                                        <thead>
                                        <tr class="uppercase">
                                            <th> Date/Time </th>
                                            <th> No of Passengers </th>
                                            <th> Payment </th>
                                            <th> Action </th>
                                        </tr>
                                        </thead>
                                        @foreach($statistics as $key => $item)
                                            <tr>
                                                <td>{{ \Carbon\Carbon::parse( $item['created_at'])->format('d-M-y H:m')  }}</td>
                                                <td>{{ $item['total_passengers'] }}</td>
                                                <td>{{ number_format($item['payment_amount'],1) }}</td>
                                                <td>
                                                    <form action="{{ url('/search-booking') }}" method="get">
                                                        <input type="hidden" name="phone" value="{{ '0'.substr($item['phone'],3) }}" >
                                                        <input type="hidden" name="travel_date" value="{{ $item['date_of_travel'] }}" >
                                                        <input type="hidden" name="origin_station" value="{{ $item['source'] }}" >
                                                        <input type="hidden" name="destination_station" value="{{ $item['destination'] }}" >
                                                        <input type="hidden" name="destination_station" value="{{ $item['destination'] }}" >
                                                        <button class="btn green" type="submit">View</button>
                                                    </form>
                                                </td>
                                            </tr>

                                        @endforeach
                                    </table>
{{--                                    {{ $statistics['tabledata']->links() }}--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="portlet light ">
                            <div class="portlet-title tabbable-line">
                                <div class="caption">
                                    <i class="icon-globe font-dark hide"></i>
                                    <span class="caption-subject font-dark bold uppercase">Tickets' Feed</span>
                                </div>
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_1" class="active" data-toggle="tab"> </a>
                                    </li>
                                    <li>
                                        <a href="#tab_1_2" data-toggle="tab"> </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="portlet-body">
                                <!--BEGIN TABS-->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1_1">
                                        <div class="scroller" style="height: 570px;" data-always-visible="1" data-rail-visible="0">
                                            <ul class="feeds">

                                                @foreach($statistics as $passenger)
                                                    <li>
                                                        <div class="col1">
                                                            <div class="cont">
                                                                <div class="cont-col1">
                                                                    <div class="label label-sm label-success">
                                                                        <i class="fa fa-bell-o"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="cont-col2">
                                                                    <div class="desc"> Ticket NO {{ ucwords($passenger['ticket_no']) }}  has been
                                                                        <span class="label label-sm label-info">

                                                                             @if($passenger['ticket_status'] == \Sgr\Models\Passanger::TICKET_PLACED)
                                                                                Placed
                                                                            @elseif($passenger['ticket_status']  == \Sgr\Models\Passanger::TICKET_CONFIRMED)
                                                                                Booked
                                                                            @elseif($passenger['ticket_status']  == \Sgr\Models\Passanger::TICKET_CANCELED_NO_PAY)
                                                                                Cancelled | No Payment
                                                                            @elseif($passenger['ticket_status']  == \Sgr\Models\Passanger::TICKET_CANCELED_PAID)
                                                                                Cancelled | Paid
                                                                            @elseif($passenger['ticket_status']  == \Sgr\Models\Passanger::TICKET_REFUNDED)
                                                                                Refunded
                                                                            @endif
                                                                                 {{ \Carbon\Carbon::parse($passenger['created_at'])->diffForHumans() }}
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col2">
                                                            <div class="date">
                                                                <span class="label label-sm label-primary">
                                                                        <a href="{{ url('tickets/show/'. $passenger['id'] ) }}" class="btn green btn-xs" type="submit">view</a>
                                                                    </span>
                                                               </div>
                                                        </div>
                                                    </li>
                                                    @endforeach

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_1_2">
                                        <div class="scroller" style="height: 290px;" data-always-visible="1" data-rail-visible1="1">
                                            <ul class="feeds">
                                                <li>
                                                    <a href="javascript:;">
                                                        <div class="col1">
                                                            <div class="cont">
                                                                <div class="cont-col1">
                                                                    <div class="label label-sm label-success">
                                                                        <i class="fa fa-bell-o"></i>
                                                                    </div>
                                                                </div>
                                                                <div class="cont-col2">
                                                                    <div class="desc"> New user registered </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col2">
                                                            <div class="date"> Just now </div>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

