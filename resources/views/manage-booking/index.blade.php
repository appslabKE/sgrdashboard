@extends('layouts.main')

@section('css')

@endsection
@section('content')

    <div class="page-content-wrapper">
            <div class="page-content">

                {{--@component('components.breadcump')--}}

                    {{--@slot('title')--}}
                        {{--Managing Bookings--}}
                    {{--@endslot--}}

                    {{--@slot('span')--}}
                         {{--tickets & bookings--}}
                    {{--@endslot--}}

                    {{--manage-bookings--}}
                        {{--@slot('menu')--}}
                        {{--@endslot--}}

                {{--@endcomponent--}}

                <div class="search-page search-content-1">

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4>Booking Search</h4>
                        </div>
                        <div class="panel-body">
                            @if($errors)
                                @foreach($errors->all() as $error)
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close white" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">close</span></button>
                                        <strong>Error! </strong>{{ $error }}
                                    </div>
                                @endforeach
                            @endif
                            <form action="{{ url('search-booking') }}" method="get">
                                    <div class="row">
                                        <div class="col-sm-3 col-xs-6 form-group">
                                            <label for="origin_station">From</label>
                                            <select name="origin_station" onclick="selectStation(this)" id="origin_station" class="form-control">
                                                <option value="">Select Station</option>
                                                @foreach($stations as $station)

                                                    <option  {{ $inputs != null && $inputs['origin_station'] == $station->station_name ? 'selected'  : ''}} value="{{$station->station_name}}">{{ ucwords($station->station_name) }}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-3 col-xs-6 form-group">
                                            <label for="destination_station">To</label>
                                            <select name="destination_station" onchange="selectStation(this)" id="destination_station" class="form-control">
                                                <option value="">Select Station</option>
                                                @foreach($stations as $station)

                                                    <option {{ $inputs != null && $inputs['destination_station'] == $station->station_name ? 'selected'  : ''}} value="{{$station->station_name}}">{{ ucwords($station->station_name) }}</option>

                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-3 col-xs-6 form-group">
                                            <label>Passenger Phone Number</label>
                                            <input name="phone" type="text" value="{{ $inputs != null && isset($inputs['phone']) ? $inputs['phone'] : ''}}" class="form-control" placeholder="0700000000">
                                        </div>
                                        {{--<div class="col-sm-3 col-xs-6 form-group ">--}}
                                            {{--<label for="train_no">Train NO</label>--}}
                                            {{--<input name="train_no" autocomplete="off" id="train_no" type="text"  placeholder="Train number" value="{{ $inputs != null ? $inputs['train_no'] : ''}}" class="form-control">--}}
                                        {{--</div>--}}

                                        {{--<div class="col-sm-3 col-xs-6 form-group ">--}}
                                            {{--<label for="coach_no">Coach NO</label>--}}
                                            {{--<input name="coach_no" autocomplete="off" id="coach_no" type="text"  placeholder="Coach number" value="{{ $inputs != null ? $inputs['coach_no'] : ''}}" class="form-control">--}}
                                        {{--</div>--}}

                                        <div class="col-sm-3 form-group">
                                            <label>Travel Date </label>
                                            <input name="travel_date" id="travel_date" type="text" placeholder="Travel date" value="{{ $inputs != null && isset($inputs['travel_date'] ) ? $inputs['travel_date'] : ''}}" class="form-control datepicker">
                                        </div>

                                        <div class="col-sm-3 col-xs-6 form-group ">
                                            <label for="seat_no">Seat NO</label>
                                            <input name="seat_no" id="seat_no" type="text"  placeholder="Seat number" value="{{ $inputs != null && isset($inputs['seat_no']) ? $inputs['seat_no'] : ''}}" class="form-control">
                                        </div>

                                        <div class="col-sm-3 col-xs-6 form-group ">
                                            <label for="ticket_no">Ticket NO</label>
                                            <input name="ticket_no" id="ticket_no" type="text" placeholder="Ticket number" value="{{ $inputs != null && isset($inputs['ticket_no'])? $inputs['ticket_no'] : ''}}" class="form-control">
                                        </div>

                                        <div class="col-sm-3 col-xs-6 form-group ">
                                            <label for="booking_no">Booking NO</label>
                                            <input name="booking_no" autocomplete="off" id="booking_no" type="text" placeholder="Booking number" value="{{ $inputs != null && isset($inputs['booking_no']) ? $inputs['booking_no'] : ''}}" class="form-control">
                                        </div>

                                        <div class="col-sm-3 col-xs-6 form-group">
                                            <label for="class_id">Class</label>
                                            <select name="class_id" id="class_id" class="form-control">
                                                <option value="">Select class</option>
                                                @foreach(\Sgr\Models\TrainClass::all() as $train_class)
                                                    <option {{ ($inputs != null && isset($inputs['class_id'] )) && $inputs['class_id'] == $train_class->class_name ? 'selected'  : ''}} value="{{ $train_class->class_name  }}">{{ ucwords($train_class->class_name) }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="col-sm-3 col-xs-6 form-group">
                                            <label for="payment_status">Payment Status</label>
                                            <select name="payment_status" id="payment_status" class="form-control">
                                                <option value="">Select Payment Status</option>
                                                <option value="{{ \Sgr\Models\Payment::PAYMENT_SUCCESSFUL }}">{{ \Sgr\Models\Payment::PAYMENT_SUCCESSFUL }}</option>
                                                <option value="{{ \Sgr\Models\Payment::PAYMENT_FAILED }}">{{ \Sgr\Models\Payment::PAYMENT_FAILED }}</option>
                                                <option value="{{ \Sgr\Models\Payment::PAYMENT_PENDING }}">{{ \Sgr\Models\Payment::PAYMENT_PENDING }}</option>
                                            </select>
                                        </div>

                                        {{--<div class="col-sm-3 col-xs-6 form-group">--}}
                                            {{--<label for="booking_status">Booking Status</label>--}}
                                            {{--<select name="booking_status" id="booking_status" class="form-control">--}}
                                                {{--<option value="">Select Payment Status</option>--}}
                                                {{--<option value="{{ \Sgr\Models\Booking::BOOKING_SUCCESSFUL }}"> Booked</option>--}}
                                                {{--<option value="{{ \Sgr\Models\Booking::BOOKING_UNSUCCESSFUL }}">Pending</option>--}}
                                                {{--<option value="{{ \Sgr\Models\Booking::BOOKING_CANCELED }}">Canceled</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}

                                        <div class="col-sm-3 col-xs-6 form-group">
                                            <label for="ticket_status">Ticket Status</label>
                                            <select name="ticket_status" id="ticket_status" class="form-control">
                                                <option value="">Select Ticket Status</option>
                                                <option value="{{ \Sgr\Models\Passanger::TICKET_PLACED }}">Pending</option>
                                                <option value="{{ \Sgr\Models\Passanger::TICKET_CONFIRMED }}">Booked</option>
                                                <option value="{{ \Sgr\Models\Passanger::TICKET_CANCELED_NO_PAY }}">Canceled Without Payment</option>
                                                <option value="{{ \Sgr\Models\Passanger::TICKET_CANCELED_PAID }}">Canceled With Payment</option>
                                                <option value="{{ \Sgr\Models\Passanger::TICKET_REFUNDED }}"> Refunded</option>
                                            </select>
                                        </div>

                                        {{--<div class="col-sm-3 col-xs-6 form-group">--}}
                                            {{--<label for="fulfilment_status">Fulfilment Status</label>--}}
                                            {{--<select name="fulfilment_status" id="fulfilment_status" class="form-control">--}}
                                                {{--<option value="">Select Fulfilment Status</option>--}}
                                                {{--<option {{ $inputs != null && $inputs['fulfilment_status'] ==\Sgr\Models\Passanger::FULLFILMENT_PLACED  ? 'selected'  : ''}} value="{{ \Sgr\Models\Passanger::FULLFILMENT_PLACED }}">{{ \Sgr\Models\Passanger::FULLFILMENT_PLACED }}</option>--}}
                                                {{--<option {{ $inputs != null && $inputs['fulfilment_status'] ==\Sgr\Models\Passanger::FULLFILMENT_BOOKED  ? 'selected'  : ''}} value="{{ \Sgr\Models\Passanger::FULLFILMENT_BOOKED }}">{{ \Sgr\Models\Passanger::FULLFILMENT_BOOKED }}</option>--}}
                                                {{--<option {{ $inputs != null && $inputs['fulfilment_status'] == \Sgr\Models\Passanger::FULLFILMENT_ONTRANSIT ? 'selected'  : ''}} value="{{ \Sgr\Models\Passanger::FULLFILMENT_ONTRANSIT }}">{{\Sgr\Models\Passanger::FULLFILMENT_ONTRANSIT }}</option>--}}
                                                {{--<option {{ $inputs != null && $inputs['fulfilment_status'] == \Sgr\Models\Passanger::FULLFILMENT_ARRIVED ? 'selected'  : ''}} value="{{ \Sgr\Models\Passanger::FULLFILMENT_ARRIVED }}">{{\Sgr\Models\Passanger::FULLFILMENT_ARRIVED }}</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                        <div class="col-sm-3 col-xs-6 form-group ">
                                            <label for="booking_date">Booking date</label>
                                            <input name="booking_date" autocomplete="off" id="booking_date" type="text" placeholder="Booking date" value="{{ $inputs != null && isset( $inputs['booking_date'])? $inputs['booking_date'] : ''}}" class="datepicker form-control date-picker">
                                        </div>
                                        <div class="col-sm-3 col-xs-6 pull-right ">
                                            <br>
                                            <button style="width: 100%" class="btn blue center-block uppercase bold" type="submit">Search</button>
                                        </div>
                                    </div>
                            </form>
                        </div>
                    </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase">Search Results</span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table id="bookingtable" class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                                        <thead>
                                        <tr>
                                            <th class="all">Booking NO</th>
                                            <th class="all">Passenger</th>
                                            <th class="all">NATIONAL ID</th>
                                            <th class="all">Ticket NO:</th>
                                            <th class="all">Phone NO</th>
                                            {{--<th class="all">Route</th>--}}
                                            <th class="all">From</th>
                                            <th class="all">To</th>
                                            <th class="all">Traveling Date</th>
                                            <th class="all">Departure</th>
                                            {{--<th style="width: auto !important;">Arrival</th>--}}
                                            {{--<th class="min-tablet">Train NO</th>--}}
                                            {{--<th class="min-tablet">Coach NO</th>--}}
                                            <th class="all">Seat NO</th>
                                            <th class="all">Ticket Type</th>
                                            <th class="all">Booking Channel</th>
                                            <th class="all">Class</th>
                                            <th class="all">Fare</th>
                                            <th class="all">Booking Date</th>
                                            <th class="all">Payment Status</th>
                                            <th class="all">Ticket Status</th>
                                            <th class="all">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($tickets as $ticket)
                                            <tr>
                                                <td >{{ $ticket['trade_id']}}</td>
                                                <td >{{ ucwords($ticket['name'])}}</td>
                                                <td >{{ $ticket['id_number']}}</td>
                                                <td >{{ $ticket['ticket_no']}}</td>
                                                <td >{{ $ticket['phone']}}</td>
{{--                                                <td>{{ $ticket['route'] }}</td>--}}
                                                <td >{{ $ticket['source'] }}</td>
                                                <td >{{ $ticket['destination'] }}</td>
                                                <td >{{ \Carbon\Carbon::parse($ticket['date_of_travel'])->format('d, M Y') }}</td>
                                                <td >{{ date('g:i:A', strtotime($ticket['departure_time']))}}</td>
{{--                                                <td >{{ \Carbon\Carbon::parse($ticket['arrival_time'])->format('H.m') }}</td>--}}
{{--                                                <td>{{ $ticket['train_no'] }}</td>--}}
{{--                                                <td>{{ $ticket['coach_no'] }}</td>--}}
                                                <td >{{ $ticket['seat_no'] }}</td>
                                                <td >{{ $ticket['ticket_type'] }}</td>
                                                <td >{{ $ticket['booking_channel'] == 0 ? 'USSD' : 'WEB'}}</td>
                                                <td >{{ $ticket['class'] }}</td>
                                                <td >{{ $ticket['payment_amount'] }}</td>
                                                <td>{{ \Carbon\Carbon::parse($ticket['booking_date'])->format('d, M Y') }}</td>
                                                <td >{{ ucwords($ticket['payment_status'])}}</td>
                                                <td >{{ $ticket['ticket_status'] }}</td>
                                                <td >
                                                    <a class="btn btn-primary btn-sm btn-outline sbold" data-toggle="modal" href="#large{{$ticket['id']}}">
                                                        <i class="fa fa-eye"></i> View </a>

                                                    <div class="modal fade bs-modal-lg" id="large{{$ticket['id']}}" tabindex="-1" role="dialog" aria-hidden="true">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                                    <h4 class="modal-title"><strong>Name </strong>{{ $ticket['name']}}
                                                                        <br><strong>From </strong>{{ $ticket['source']}}
                                                                        <br><strong>To</strong> {{ $ticket['destination'] }}</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped table-bordered">
                                                                            <tbody>
                                                                            <tr>
                                                                                <td><strong>Booking No</strong> {{ $ticket['trade_id'] }}</td>
                                                                                <td><strong>Ticket Type</strong> {{ $ticket['ticket_type'] }}</td>
                                                                                <td><strong>Ticket Number</strong> {{ $ticket['ticket_no'] }}</td>

                                                                            </tr>
                                                                            <tr>

                                                                                <td><strong>Booking Date</strong> {{ \Carbon\Carbon::parse($ticket['booking_date'])->format('d, M Y') }}</td>
                                                                                <td><strong>Traveling Date</strong> {{ \Carbon\Carbon::parse($ticket['date_of_travel'])->format('d, M Y') }}</td>
                                                                                <td><strong>Departure Time</strong> {{ date('g:i:A', strtotime($ticket['departure_time'])) }}</td>
                                                                                {{--                                                                                <td><strong>Fulfilment Status </strong>{{ $ticket['fulfillment_status'] }}</td>--}}
                                                                            </tr>
                                                                            <tr>
                                                                                <td><strong>Passenger</strong> {{ $ticket['name'] }}</td>
                                                                                <td><strong>ID No</strong> {{ $ticket['id_number'] }}</td>
                                                                                <td><strong>Phone NO</strong> {{ $ticket['phone'] }}</td>
{{--                                                                                <td><strong>Arrival Time </strong> {{ $ticket['arrival_time'] }}</td>--}}

                                                                            </tr>
                                                                            <tr>
                                                                                <td><strong>Booking Channel</strong> {{ $ticket['booking_channel'] == 0 ? 'USSD' : 'WEB'}}</td>
                                                                                <td><strong>Class</strong> {{ $ticket['class'] }}</td>
                                                                                <td><strong> Fare </strong> {{ $ticket['payment_amount'] }}</td>
{{--                                                                                <td><strong>Arrival Time </strong> {{ $ticket['arrival_time'] }}</td>--}}

                                                                            </tr>
                                                                            <tr>
                                                                                <td><strong>From</strong> {{ $ticket['source'] }}</td>
                                                                                <td><strong>To </strong> {{ $ticket['destination'] }}</td>
                                                                                <td><strong> Seat NO </strong> {{ $ticket['seat_no'] }}</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td><strong>Payment Status </strong>{{ ucwords($ticket['payment_status'])}}</td>
                                                                                {{--<td><strong>Booking Status </strong>--}}
                                                                                    {{--@if($ticket['booking_status'] == \Sgr\Models\Booking::BOOKING_SUCCESSFUL)--}}
                                                                                        {{--Successful--}}
                                                                                    {{--@else--}}
                                                                                        {{--Unsuccessful--}}
                                                                                    {{--@endif--}}

                                                                                {{--</td>--}}
                                                                                <td><strong>Ticket Status </strong>
                                                                                    @if($ticket['ticket_status'] == \Sgr\Models\Passanger::TICKET_PLACED)
                                                                                        Placed
                                                                                    @elseif($ticket['ticket_status'] == \Sgr\Models\Passanger::TICKET_CONFIRMED)
                                                                                        Confirmed
                                                                                    @elseif($ticket['ticket_status'] == \Sgr\Models\Passanger::TICKET_CANCELED_NO_PAY)
                                                                                        Cancelled
                                                                                    @elseif($ticket['ticket_status'] == \Sgr\Models\Passanger::TICKET_CANCELED_PAID)
                                                                                        Cancelled
                                                                                    @elseif($ticket['ticket_status'] == \Sgr\Models\Passanger::TICKET_REFUNDED)
                                                                                        Refunded
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                                {{--<div class="modal-footer">--}}
                                                                    {{--<div style="overflow: auto !important;">--}}
                                                                        {{--<button type="button" class="btn btn-sm  pull-left dark btn-outline" data-dismiss="modal">Close</button>--}}
                                                                        {{--<a href="{{ url('actions/cancel-ticket/'.$ticket['id']) }}" type="button" class="btn btn-sm  red">Cancel</a>--}}
                                                                        {{--<a href="{{ url('actions/re-send-sms/'.$ticket['id']) }}" type="button" class="btn btn-sm purple">Resend SMS</a>--}}
                                                                        {{--<a href="{{ url('actions/refund/'.$ticket['id'])  }}" type="button" class="btn btn-sm yellow">Refund</a>--}}
                                                                        {{--<a href="{{ url('actions/re-book/'.$ticket['id']) }}" type="button" class="btn btn-sm dark">Re-book</a>--}}
                                                                        {{--<a href="{{ url('actions/confirm-refund/'.$ticket['id']) }}" type="button" class="btn btn-sm  btn-success">Confirm Refund</a>--}}
                                                                        {{--<a href="{{ url('actions/confirm-payment/'.$ticket['id']) }}" type="button" class="btn btn-sm  btn-success">Confirm Payment</a>--}}
                                                                        {{--<a href="{{ url('actions/feedback/'.$ticket['id']) }}" type="button" class="btn btn-sm  btn-warning">Feedback</a>--}}
                                                                    {{--</div>--}}
                                                                {{--</div>--}}
                                                            </div>
                                                            <!-- /.modal-content -->
                                                        </div>
                                                        <!-- /.modal-dialog -->
                                                    </div>
                                                    {{--</div>--}}

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
{{--                                {{ $tickets->links() }}--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

@endsection

@section('scripts')

            <script>
                $('.datepicker').datepicker(
                    {
                        autoclose : true
                    }
                );

                function selectStation(station) {

                    var destination = $('#destination_station');
                    var origin = $('#origin_station');
                    var origin_station = origin.val();
                    var destination_station = destination.val();

                    if (origin_station === destination_station && origin_station != '' && destination_station != ''){

                        console.log(origin_station, destination_station);
                        alert('From station cannot be the same as To station');
                        destination.val('');
                        origin.val('');
                    }

                }
            </script>

@endsection

