@extends('layouts.main')

@section('css')

@endsection
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">

            @component('components.breadcump')

                @slot('title')
                    Roles
                @endslot

                @slot('span')
                    add role
                @endslot
                    roles
                @slot('menu')
                    <div class="page-toolbar">
                        <div class="btn-group pull-right">
                            <a href="{{ url()->previous() }}" class="btn btn-fit-height grey-salt" > Back
                                {{--<i class="fa fa-angle-down"></i>--}}
                            </a>
                        </div>
                    </div>
                @endslot

            @endcomponent

            <div class="search-page search-content-1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase">Add Role</span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <form action="{{ route('roles.store') }}" method="post">
                                {{ csrf_field() }}

                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="role">Roles</label>
                                            <input name="role" id="role" value="{{ old('role')  }}" required type="text" class="form-control">
                                            @if ($errors->has('role'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('role') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <label for="description">Description</label>
                                            <input name="description" id="description" value="{{ old('description') }}" type="text" class="form-control">
                                            @if($errors->has('description'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group col-sm-6  pull-right">
                                            <br>
                                            <button type="submit" class="btn pull-right btn-primary">Add Role</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

@endsection

@section('scripts')

@endsection