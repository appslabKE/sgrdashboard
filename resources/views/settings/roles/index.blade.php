@extends('layouts.main')

@section('css')

@endsection
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">

            @component('components.breadcump')

                @slot('title')
                    Roles
                @endslot

                @slot('span')
                    manage roles
                @endslot

                roles

                @slot('menu')
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <a href="{{ route('roles.create') }}" class="btn btn-fit-height green" > Add Role
                                    {{--<i class="fa fa-angle-down"></i>--}}
                                </a>
                            </div>
                        </div>
                @endslot

            @endcomponent

            <div class="search-page search-content-1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase">Roles</span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                                    <thead>
                                    <tr>
                                        <th >NO:</th>
                                        <th >Role</th>
                                        <th >Description</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($roles as $role)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ ucwords($role->role) }}</td>
                                            <td>{{ ucfirst($role->description) }}</td>
                                            <td class="text-center">
                                                @if($role->id !=1)
                                                <form action="{{ route('roles.destroy', $role->id) }}" method="post">
                                                    {{ csrf_field() }}
                                                    {{ method_field('delete') }}
                                                    <a href="{{ route('roles.edit', $role->id) }}" class="btn green">
                                                        <i class="fa fa-pencil"></i></a>

                                                    <button class="btn red"><i class="fa fa-trash"></i></button>
                                                </form>
                                                    @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('scripts')

@endsection

