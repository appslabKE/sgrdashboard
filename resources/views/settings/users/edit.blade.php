@extends('layouts.main')

@section('css')

@endsection
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">

            @component('components.breadcump')

                @slot('title')
                    Users
                @endslot

                @slot('span')
                    edit user
                @endslot
users
                @slot('menu')
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <a href="{{ url()->previous() }}" class="btn btn-fit-height grey-salt" > Back
                                    {{--<i class="fa fa-angle-down"></i>--}}
                                </a>
                            </div>
                        </div>
                @endslot

            @endcomponent

            <div class="search-page search-content-1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase">Edit User</span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <form action="{{ route('users.update', $user->id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('put') }}

                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="name">Full Name</label>
                                            <input name="name" id="name" value="{{ ucwords($user->name)  }}" required type="text" class="form-control">
                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <label for="phone">Phone Number</label>
                                            <input name="phone" id="phone" value="{{ $user->phone }}" required type="tel" class="form-control">
                                            @if($errors->has('phone'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <label for="email">Email Address</label>
                                            <input id="email" name="email" type="text" value="{{ $user->email }}" class="form-control">
                                            @if($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <label for="password">Password</label>
                                            <input id="password" name="password" value="{{ old('password') }}" required type="text" class="form-control">
                                            @if($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <label for="role">Role</label>
                                            <select required name="role" id="role" class="form-control">
                                                <option value="">Select Role</option>
                                                @foreach($roles as $role)
                                                    <option {{ $user->id == $role->id ? 'selected' : '' }} value="{{ $role->id }}">{{ ucwords($role->role) }}</option>
                                                @endforeach
                                            </select>
                                            @if($errors->has('role_id'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('role_id') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group col-sm-6">
                                            <br>
                                            <button type="submit" class="btn pull-right btn-primary">Edit User</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('scripts')

@endsection