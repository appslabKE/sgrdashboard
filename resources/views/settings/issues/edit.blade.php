@extends('layouts.main')

@section('css')

@endsection
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">

            @component('components.breadcump')

                @slot('title')
                    Issue
                @endslot

                @slot('span')
                    edit issue
                @endslot

                    issue
                @slot('menu')
                        {{--<div class="page-toolbar">--}}
                            {{--<div class="btn-group pull-right">--}}
                                {{--<a href="{{ url()->previous() }}" class="btn btn-fit-height grey-salt" > Back--}}
                                    {{--<i class="fa fa-angle-down"></i>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                @endslot

            @endcomponent

            <div class="search-page search-content-1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase">Edit Issue</span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <form action="{{ route('issues.update', $issue->id) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('put') }}

                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="form-group col-sm-6">
                                            <label for="role">Issue Type</label>
                                            <input name="role" id="role" value="{{ ucwords($issue->issue)  }}" required type="text" class="form-control">
                                            @if ($errors->has('role'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('role') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group col-sm-6">
                                            <label for="description">Description</label>
                                            <input name="description" id="description" value="{{ $issue->description }}" type="text" class="form-control">
                                            @if($errors->has('description'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('description') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="form-group col-sm-6  pull-right">
                                            <br>
                                            <div class="pull-right">
                                                <a href="{{ url()->previous() }}" class="btn btn-fit-height red" > Back
                                                    {{--<i class="fa fa-angle-down"></i>--}}
                                                </a>
                                                <button type="submit" class="btn btn-primary">Edit</button>

                                            </div>
                                            </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('scripts')

@endsection