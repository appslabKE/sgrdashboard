@component('mail::message')

The remaining SMS in KenyaSGR system is {{$sms_count}}.

@component('mail::button', ['url' => 'http://dashboard.kenyasgr.com'])
Dashboard
@endcomponent

<strong style="color: tomato">Do not reply to this email</strong> <br>
Thanks,<br>
KenyaSGR
@endcomponent
