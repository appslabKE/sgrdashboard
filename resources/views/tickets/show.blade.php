@extends('layouts.main')

@section('css')

@endsection
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">

            @component('components.breadcump')

                @slot('title')
                    Ticket
                @endslot

                @slot('span')
                        detail
                @endslot
                    ticket detail
                @slot('menu')
                    <div class="page-toolbar">
                        <div class="btn-group pull-right">
                            <a href="{{ url()->previous() }}" class="btn btn-fit-height grey-salt" > Back
                                {{--<i class="fa fa-angle-down"></i>--}}
                            </a>
                        </div>
                    </div>
                @endslot

            @endcomponent

            <div class="search-page search-content-1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-daDetailsrk">
                                    <span class="caption-subject bold uppercase">Update Ticket</span>
                                </div>
                                <div class="tools"> </div>
                            </div>

                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tr>
                                                    {{--<td><strong>Booking NO </strong>{{ $ticket->booking->trade_id }}</td>--}}
                                                    <td><strong>Passenger </strong>{{ $ticket->name }}</td>
                                                    <td><strong>ID No</strong> {{ $ticket->id_no }}</td>
                                                   <td><strong>Ticket NO </strong>{{ $ticket->ticket_no }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Mobile No </strong>{{ $ticket->booking->phone }}</td>
                                                    <td><strong>Route </strong>{{ $ticket->booking->route}}</td>
                                                    <td><strong>Travel Date </strong>{{ $ticket->booking->date_of_travel  }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Departure </strong>{{ $ticket->booking->departure_time }}</td>
                                                    <td><strong>Arrival </strong>{{ $ticket->booking->arrival_time }}</td>
                                                    <td><strong>Seat NO </strong>{{ $ticket->seat_no }}</td>
                                                    {{--                                                    <td><strong>Train NO </strong>{{ $ticket->booking->train_id }}</td>--}}
{{--                                                    <td><strong>Coach NO </strong>{{ $ticket->coach_id }}</td>--}}
                                                </tr>
                                                <tr>
                                                    <td><strong>Ticket Type </strong>{{ $ticket->ticket_type }}</td>
                                                    <td><strong>Class </strong>{{ $ticket->class }}</td>
                                                    <td><strong>Fare KSH: </strong>{{ number_format($ticket->amount_paid != null ? $ticket->amount_paid : 0,2)  }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Booking Date </strong>{{ $ticket->booking->createdAt }}</td>
                                                    <td><strong>Payment Date </strong>{{ $ticket->booking->payment->createdAt }}</td>
                                                    <td><strong>Payment Code </strong>{{ $ticket->booking->payment->provider_transaction_id }}</td>
                                                </tr>
                                                <tr>
                                                    <td><strong>Booking Status </strong>
                                                        {{ $ticket->booking->booking_status }}
                                                    </td>
                                                    <td><strong>Payment Status </strong>{{ $ticket->booking->payment->payment_status }}</td>
                                                    <td><strong>Ticket Status </strong>
                                                        {{ $ticket->ticket_status }}
                                                        </td>
                                                    </tr>
                                                <tr>
                                                    <td><strong>Fulfilment Status </strong>{{ $ticket->fulfillment_status }}</td>
                                                    <td><strong>Booking Channel </strong>{{ $ticket->booking->booking_channel == 'ussd' ? 'USSD' : 'Web' }}</td>

                                                </tr>
                                            </table>
                                        </div>
                                        {{--<div style="overflow: auto !important;">--}}
                                            {{--<a href="{{ url()->previous() }}" class="btn  pull-left red" data-dismiss="modal">Back</a>--}}
                                            {{--<div class="pull-right">--}}

                                                {{--<a href="{{ url('actions/cancel-ticket/'.$ticket->id) }}" type="button" class="btn red">Cancel</a>--}}
                                                {{--<a href="{{ url('actions/re-send-sms/'.$ticket->id) }}" type="button" class="btn  purple">Resend SMS</a>--}}
                                                {{--<a href="{{ url('actions/refund/'.$ticket->id)  }}" type="button" class="btn  yellow">Refund</a>--}}
                                                {{--<a href="{{ url('actions/re-book/'.$ticket->id) }}" type="button" class="btn  dark">Re-book</a>--}}
                                                {{--<a href="{{ url('actions/refund/'.$ticket->id) }}" type="button" class="btn   btn-success">Refund</a>--}}
                                                {{--<a href="{{ url('actions/confirm-payment/'.$ticket->id) }}" type="button" class="btn   btn-success">Confirm Payment</a>--}}
                                                {{--<a href="{{ url('actions/feedback/'.$ticket->id) }}" type="button" class="btn   btn-warning">Feedback</a>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

@endsection