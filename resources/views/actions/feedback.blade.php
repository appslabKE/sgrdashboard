@extends('layouts.main')

@section('css')

@endsection
@section('content')

<div class="page-content-wrapper">
    <div class="page-content">

        @component('components.breadcump')

        @slot('title')
        Feedback
        @endslot

        @slot('span')
        feedback
        @endslot
        feedback
        @slot('menu')
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a href="{{ url()->previous() }}" class="btn btn-fit-height grey-salt" > Back
                    {{--<i class="fa fa-angle-down"></i>--}}
                </a>
            </div>
        </div>
        @endslot

        @endcomponent

        <div class="search-page search-content-1">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <span class="caption-subject bold uppercase">{{ $user->name != null ? ucwords($user->name)."'s" : ''}} feedback</span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <form action="{{ url('actions/feedback/save') }}" method="post">
                            {{ csrf_field() }}

                            <div class="portlet-body">

                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <label for="subject">Customer Rating (1 - 5)</label>
                                        <input type="number" min="1" max="5" class="form-control" required value="{{ old('subject') }}" name="subject" id="subject" >
                                        @if ($errors->has('subject'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('subject') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <input type="hidden" name="passenger_id" value="{{ $user->id }}">

                                    <div class="form-group col-sm-12">
                                        <label for="feedback">Feedback</label>
                                        <textarea name="feedback" required class="form-control" id="feedback" cols="30" ></textarea>
                                        @if($errors->has('feedback'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('feedback') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <br>
                                        <a href="{{ url()->previous() }}" class="btn pull-left btn-warning">Back</a>
                                        <button type="submit" class="btn pull-right btn-primary">Add</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection