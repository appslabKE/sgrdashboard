@extends('layouts.main')

@section('css')

@endsection
@section('content')

<div class="page-content-wrapper">
    <div class="page-content">

        @component('components.breadcump')

        @slot('title')
        Re-send SMS
        @endslot

        @slot('span')
        resend SMS
        @endslot
        actions
        @slot('menu')
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a href="{{ url()->previous() }}" class="btn btn-fit-height grey-salt" > Back
                    {{--<i class="fa fa-angle-down"></i>--}}
                </a>
            </div>
        </div>
        @endslot

        @endcomponent

        <div class="search-page search-content-1">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <span class="caption-subject bold uppercase">RESEND SMS TO {{ ucwords($user->name) }}</span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <form action="{{ url('actions/send-sms/save') }}" method="post">
                            {{ csrf_field() }}

                            <div class="portlet-body">

                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <label for="phone_number">Phone Number</label>
                                        <input type="tel" class="form-control" required value="{{ $user->booking->phone }}" name="phone_number" id="phone_number" >
                                        @if ($errors->has('phone_number'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('phone_number') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <input type="hidden" name="ticket_id" value="{{ $user->id }}">

                                    <div class="form-group col-sm-12">
                                        <label for="message">Message</label>
                                        <textarea name="message" required class="form-control" id="message" cols="30" ></textarea>
                                        @if($errors->has('message'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('message') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <br>
                                        <a href="{{ url()->previous() }}" class="btn pull-left btn-danger">Back</a>
                                        <button type="submit" class="btn pull-right btn-primary">Re-send SMS</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection