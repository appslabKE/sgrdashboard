@extends('layouts.main')

@section('css')

@endsection
@section('content')

<div class="page-content-wrapper">
    <div class="page-content">

        @component('components.breadcump')

        @slot('title')
        Refund
        @endslot

        @slot('span')
        confirm refund
        @endslot
        refund
        @slot('menu')
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a href="{{ url()->previous() }}" class="btn btn-fit-height grey-salt" > Back
                    {{--<i class="fa fa-angle-down"></i>--}}
                </a>
            </div>
        </div>
        @endslot

        @endcomponent

        <div class="search-page search-content-1">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <span class="caption-subject bold uppercase">Refund to {{ ucwords($user->name) }}</span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <form action="{{ url('actions/refund/save') }}" method="post">
                            {{ csrf_field() }}

                            <div class="portlet-body">

                                @if($user->refund_status == 1)
                                    <h4 >KSH : {{ number_format($user->refund->refund_amount, 2) }} was refunded to {{ ucwords($user->name) }}
                                        , on {{ \Carbon\Carbon::parse($user->refund->created_at)->format('d-M-y') }} via {{ ucwords($user->refund->refund_method) }}
                                    </h4>

                                    @else
                                    <h4>No Refund Issued</h4>

                                    @endif
                                <div class="row">


                                    <div class="form-group col-sm-12">
                                        <br>
                                        <a href="{{ url()->previous() }}" class="btn pull-left btn-danger">Back</a>
                                        {{--<button type="submit" class="btn pull-right btn-primary">Confirm Refund</button>--}}
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection