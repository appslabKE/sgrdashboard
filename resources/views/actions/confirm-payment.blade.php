@extends('layouts.main')

@section('css')

@endsection
@section('content')

<div class="page-content-wrapper">
    <div class="page-content">

        @component('components.breadcump')

        @slot('title')
        Payment
        @endslot

        @slot('span')
        confirm payment
        @endslot
        payment
        @slot('menu')
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a href="{{ url()->previous() }}" class="btn btn-fit-height grey-salt" > Back
                    {{--<i class="fa fa-angle-down"></i>--}}
                </a>
            </div>
        </div>
        @endslot

        @endcomponent

        <div class="search-page search-content-1">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <span class="caption-subject bold uppercase">Confirm payment by {{ ucwords($user->name) }}</span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <form action="{{ url('actions/confirm-payment/save') }}" method="post">
                            {{ csrf_field() }}

                            <div class="portlet-body">

                               <h4>{{ ucwords($user->name) }} payment status is <strong>{{ ucwords($user->booking->payment->payment_status) }}</strong></h4>
                                <hr>
                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <label for="payment_status">Change Payment Status</label>
                                        <select name="payment_status" id="payment_status" required class="form-control">
                                            <option value="">Select payment status</option>
{{--                                            @if($user->booking->payment->payment_status == \Sgr\Models\Payment::PAYMENT_PENDING)--}}
{{--                                            <option value="{{ \Sgr\Models\Payment::PAYMENT_PENDING }}">{{ \Sgr\Models\Payment::PAYMENT_PENDING }}</option>--}}
                                            {{--@endif--}}
{{--                                            @if(ucwords($user->booking->payment->payment_status) != \Sgr\Models\Payment::PAYMENT_FAILED )--}}
{{--                                            <option value="{{ \Sgr\Models\Payment::PAYMENT_FAILED }}">{{ \Sgr\Models\Payment::PAYMENT_FAILED }}</option>--}}
                                            {{--@endif--}}
{{--                                            @if(ucwords($user->booking->payment->payment_status) != \Sgr\Models\Payment::PAYMENT_SUCCESSFUL )--}}
                                            <option value="{{ \Sgr\Models\Payment::PAYMENT_SUCCESSFUL }}">{{ \Sgr\Models\Payment::PAYMENT_SUCCESSFUL }}</option>
                                            <option value="{{ \Sgr\Models\Payment::PAYMENT_FAILED }}">{{ \Sgr\Models\Payment::PAYMENT_FAILED }}</option>
                                            <option value="{{ \Sgr\Models\Payment::PAYMENT_PENDING }}">{{ \Sgr\Models\Payment::PAYMENT_PENDING }}</option>
                                            {{--@endif--}}
                                        </select>
                                        @if ($errors->has('refund_method'))
                                            <span class="help-block">
                                                    <strong>{{ $errors->first('refund_method') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <label for="">Mpesa REF</label>
                                        <input type="text" name="mpesa_ref" required class="form-control">
                                    </div>

                                    <input type="hidden" name="payment_id" value="{{ $user->booking->payment->payment_id }}">
                                    <input type="hidden" name="ticket_id" value="{{ $user->id }}">

                                    <div class="form-group col-sm-12">
                                        <br>
                                        <a href="{{ url()->previous() }}" class="btn pull-left btn-danger">Back</a>
                                        <button type="submit" class="btn pull-right btn-primary">Update</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection