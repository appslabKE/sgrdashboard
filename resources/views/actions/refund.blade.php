@extends('layouts.main')

@section('css')

@endsection
@section('content')

<div class="page-content-wrapper">
    <div class="page-content">

        @component('components.breadcump')

        @slot('title')
        Refund
        @endslot

        @slot('span')
        refund
        @endslot
        actions
        @slot('menu')
        @endslot

        @endcomponent

        <div class="search-page search-content-1">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">

                    <div class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <span class="caption-subject bold uppercase">Refund {{ ucwords($user->name) }} - Ticket NO {{ $user->ticket_no }}</span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <form action="{{ url('actions/refund/save') }}" method="post">
                            {{ csrf_field() }}

                            <div class="portlet-body">
                                <h4 style="color: red">You are about to refund {{ ucwords($user->name) }} amount worth <span style="color: black">KSH : {{ number_format($user->booking->payment->amount == null ? 0 : $user->booking->payment->amount, 2 )  }}</span></h4>

                                <div class="row">
                                    <div class="form-group col-sm-12">
                                        <label for="refund_method">Refund Method</label>
                                        <select name="refund_method" id="refund_method" required class="form-control">
                                            <option value="">Select method</option>
                                            <option value="mpesa">M-Pesa</option>
                                            <option value="bank">Bank</option>
                                            <option value="cash">Cash</option>
                                        </select>
                                        @if ($errors->has('refund_method'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('refund_method') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <input type="hidden" name="ticket_id" value="{{ $user->id }}">
                                    <input type="hidden" name="amount" value="{{ $user->booking->payment->amount }}">

                                    <div class="form-group col-sm-12">
                                        <label for="message">Note</label>
                                        <textarea name="message" required class="form-control" id="message" cols="30" ></textarea>
                                        @if($errors->has('message'))
                                        <span class="help-block">
                                                    <strong>{{ $errors->first('message') }}</strong>
                                                </span>
                                        @endif
                                    </div>

                                    <div class="form-group col-sm-12">
                                        <br>
                                        <a href="{{ url()->previous() }}" class="btn pull-left btn-danger">Back</a>
                                        <button type="submit" class="btn pull-right btn-primary">Request for Refund</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

@endsection