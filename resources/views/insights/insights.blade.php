@extends('layouts.main')

@section('css')

@endsection
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">

            @component('components.breadcump')

                @slot('title')
                    Misc
                @endslot

                @slot('span')
                    misc
                @endslot

                    misc
                    @slot('menu')
                    @endslot
            @endcomponent

            <div class="search-page search-content-1">

                <div class="row">
                    <div class="col-md-6">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase">Top Routes</span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                                    <thead>
                                    <tr>
                                        {{--<th >Route NO</th>--}}
                                        <th>Route</th>
                                        <th>Total Bookings</th>
                                        {{--<th>Action</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($insights['top_routes'] as $routes)
                                            <tr>
                                                <td>{{ $routes['route'] }}</td>
{{--                                                <td>{{ $routes['from'] }} - {{ $routes['to'] }}</td>--}}
                                                <td>{{ $routes['number_of_bookings'] }}</td>
                                                {{--<td>--}}
                                                    {{--<a href="" class="btn btn-primary"><i class="fa fa-eye"></i>View</a>--}}
                                                {{--</td>--}}
                                            </tr>
                                        @endforeach
                                    </tbody>
                                    <tfooter>
                                    <tr>
                                        {{--<th >NO</th>--}}
                                        <th>Route</th>
                                        <th>Total Bookings</th>
                                        {{--<th>Action</th>--}}
                                    </tr>
                                    </tfooter>
                                </table>
                            </div>
                        </div>
                    </div>
                        <div class="col-sm-6">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase">Top Payment Method</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                                        <thead>
                                        <tr>
                                            <th  >NO</th>
                                            <th >Payment Method</th>
                                            <th >Total Amount</th>
                                            {{--<th >Action</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($insights['top_payments'] as $payment)
                                            <tr>
                                                <td >{{ $loop->iteration }}</td>
                                                <td >{{ $payment['method'] }}</td>
                                                <td >KSH : {{ number_format($payment['amount'],2) }}</td>
                                                {{--<td >--}}
                                                {{--<a href="" class="btn btn-primary"><i class="fa fa-eye"></i>View</a>--}}
                                                {{--</td>--}}
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfooter>
                                        <tr>
                                            <th  >NO</th>
                                            <th >Payment Method</th>
                                            <th >Total Amount</th>
                                            {{--<th >Action</th>--}}
                                        </tr>
                                        </tfooter>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <span class="caption-subject bold uppercase">Top Passengers</span>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                                        <thead>
                                        <tr>
                                            <th >NO</th>
                                            <th >Phone Number</th>
                                            <th >Number of Bookings</th>
                                            <th >Total Amount</th>
                                            {{--<th >Action</th>--}}
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($insights['top_passengers'] as $passenger)
                                            <tr>
                                                <td >{{ $loop->iteration }}</td>
                                                <td >{{ ucwords($passenger['name']) }}</td>
                                                <td >{{ $passenger['number_of_bookings'] }}</td>
                                                <td >KSH : {{ number_format($passenger['amount'], 2) }}</td>
                                                {{--<td >--}}
                                                {{--<a href="" class="btn btn-primary"><i class="fa fa-eye"></i>View</a>--}}
                                                {{--</td>--}}
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfooter>
                                            <tr>
                                                <th  >NO</th>
                                                <th >Phone Number</th>
                                                <th >Number of Bookings</th>
                                                <th >Total Amount</th>
                                                {{--<th >Action</th>--}}
                                            </tr>
                                        </tfooter>
                                    </table>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

        @endsection

        @section('scripts')

@endsection

