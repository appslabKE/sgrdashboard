<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title>Insights</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="{{ asset('css/allsgr.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('js/pie.js') }}"></script>

    <link rel="shortcut icon" href="favicon.ico" /> </head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
<!-- BEGIN HEADER -->
@include('partials.topnav')
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"> </div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN SIDEBAR -->
    @include('partials.sidenav')
    <!-- END SIDEBAR -->
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <h1 class="page-title"> Insights
                <small>insights</small>
            </h1>
            <div class="page-bar">
                <ul class="page-breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="{{ url('/') }}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <span>Insights</span>
                    </li>
                </ul>
            </div>
            <div class="search-page search-content-1">
                <div class="row ">
                    <div class="col-sm-4">
                        <form action="{{ url('insights') }}" method="get">
                            <div class="row">
                                <div class="col-sm-4 form-group">
                                    <label>Group By</label>
                                    <div class="form-group">
                                        <select name="groupby" id="groupby" class="form-control">
                                            <option value="%Y-%m-%d">Daily</option>
                                            {{--<option value="%Y-%v">Weekly</option>--}}
                                            {{--<option value="%Y-%M">Monthly</option>--}}
                                            {{--<option value="%Y">Yearly</option>--}}
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-8 form-group">
                                    <label>Select Date Range</label>
                                    <div class="input-group">
                                        <input name="date_range" id="date_range" type="text" required placeholder="Date Range" value="" class="datepicker form-control date-picker">
                                        <span class="input-group-btn">
                                        <button class="btn blue uppercase bold" type="submit">Show</button>
                                    </span>
                                    </div>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-yellow"></i>
                                <span class="caption-subject font-yellow bold uppercase">Tickets</span>
                            </div>
                        </div>
                        <div class="portlet-body">

                                <h4> {{ $insights['total_booking'] }}
                                Tickets Till Date</h4>

                            <div class="row">
                                <div class="col-sm-12 chart ct-chart">
                                    <div class="panel panel-default col-sm-12" >
                                        <canvas style="padding: 50px" id="pie-chart" width="800" height="450"></canvas>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-sm-12 chart ct-chart">
                                    <div class="panel panel-default col-sm-12">
                                        <canvas id="line-chart"></canvas>
                                    </div>
                                        @php

                                                $label = [];
                                                $series = [];
                                                $revenue = [];
                                                $labels = [];
                                                $web = [];
                                                $ussd = [];
                                                $dataBookings = [];
                                                $dataRevenue = [];
                                                    foreach($insights['monthly_records'] as $insight){
                                                      array_push($label,$insight['month']);
                                                      array_push($series,$insight['total_tickets']);
                                                      array_push($revenue,$insight['total_revenue']);
                                                      array_push($web,$insight['channel_web']);
                                                      array_push($ussd,$insight['channel_ussd']);
                                                    }

                                                    foreach ($insights['insights'] as $insight){
                                                        array_push($labels,$insight['class']);
                                                        array_push($dataBookings,$insight['total_tickets']);
                                                        array_push($dataRevenue,$insight['total_revenue']);
                                                    }

                                        @endphp
                                </div>
                                                                {{--<h4>Tickets / Months - Tickets</h4>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-yellow"></i>
                                <span class="caption-subject font-yellow bold uppercase">Revenue</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <h4> KSH : {{ number_format($insights['total_net_revenue'],2)  }}
                                Revenue Till Date</h4>
                            <div class="row">
                                <div class="col-sm-12 chart ct-chart">
                                    <div class="panel panel-default col-sm-12" >
                                        <canvas style="padding: 50px" id="pie-chart1" width="800" height="450"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-sm-12 chart ct-chart">
                                    <div class="panel panel-default col-sm-12">
                                        <canvas id="line-chart1"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class=" icon-layers font-yellow"></i>
                                <span class="caption-subject font-yellow bold uppercase">channels</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-sm-12 ct-chart">
                                    <div class="panel panel-default col-sm-12">
                                        <canvas id="line-chart2"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

                <script>

                    new Chart(document.getElementById("line-chart"), {
                        type: 'line',
                        data: {
                            labels: {!! json_encode($label) !!},
                            datasets: [{
                                data: {!! json_encode($series) !!},
                                label: "Tickets",
                                borderColor: "#3e95cd",
                                fill: false
                            }
                            ]
                        },
                        options: {
                            title: {
                                display: true,
                                text: 'Tickets / Months - Tickets'
                            }
                        }
                    });

                    new Chart(document.getElementById("line-chart2"), {
                        type: 'line',
                        data: {
                            labels: {!! json_encode($label) !!},
                            datasets: [{
                                data: {!! json_encode($ussd) !!},
                                label: "USSD",
                                borderColor: "#000000",
                                fill: false
                            },{
                                data: {!! json_encode($web) !!},
                                label: "WEB",
                                borderColor: "#D70206",
                                fill: false
                            }
                            ]
                        },
                        options: {
                            title: {
                                display: true,
                                text: 'Tickets / Months - Tickets'
                            }
                        }
                    });

                    new Chart(document.getElementById("line-chart1"), {
                        type: 'line',
                        data: {
                            labels: {!! json_encode($label) !!},
                            datasets: [{
                                data: {!! json_encode($revenue) !!},
                                label: "Tickets",
                                borderColor: "#3e95cd",
                                fill: false
                            }
                            ]
                        },
                        options: {
                            title: {
                                display: true,
                                text: 'Cash / Months - Revenue'
                            }
                        }
                    });

                    new Chart(document.getElementById("pie-chart"), {
                        type: 'pie',
                        data: {
                            labels: {!! json_encode($labels) !!},
                            datasets: [{
                                label: "Population (millions)",
                                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                                data: {!! json_encode($dataBookings) !!}
                            }]
                        },
                        options: {
                            title: {
                                display: true,
                                text: 'Booking Classes'
                            }
                        }
                    });

                    new Chart(document.getElementById("pie-chart1"), {
                        type: 'pie',
                        data: {
                            labels: {!! json_encode($labels) !!},
                            datasets: [{
                                label: "Population (millions)",
                                backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                                data: {!! json_encode($dataRevenue) !!}
                            }]
                        },
                        options: {
                            title: {
                                display: true,
                                text: 'Booking Revenue'
                            }
                        }
                    });
                    
                </script>
        </div>
        </div>
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<!-- BEGIN FOOTER -->
<div class="page-footer">
    @include('partials.footer')
</div>
    <script src="{{ asset('js/allsgr.js') }}" type="text/javascript"></script>
<script>
    $(function() {

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#date_range span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        $('#date_range').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
//                        'Today': [moment(), moment()],
//                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

    });
</script>

</body>

</html>