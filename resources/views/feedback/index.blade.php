@extends('layouts.main')

@section('css')

@endsection
@section('content')

    <div class="page-content-wrapper">
        <div class="page-content">

            @component('components.breadcump')

                @slot('title')
                    Customers Feedback
                @endslot

                @slot('span')
                    view feedback
                @endslot

                feedback

                @slot('menu')

                @endslot

            @endcomponent

            <div class="search-page search-content-1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet light ">
                            <div class="portlet-title">
                                <div class="caption font-dark">
                                    <span class="caption-subject bold uppercase">Feedback</span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <div class="portlet-body">
                                <table id="bookingtable" class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                                    <thead>
                                    <tr>
                                        <th >NO:</th>
                                        <th >Customer Name</th>
                                        <th >Customer Phone No</th>
                                        <th >Customer Rating</th>
                                        <th >Feedback</th>
                                        <th >Added By</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($feedbacks as $feedback)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ ucwords($feedback->passenger->name) }}</td>
                                            <td>{{ ucwords($feedback->passenger->booking->phone) }}</td>
                                            <td>{{ $feedback->subject }} / 5</td>
                                            <td>{{ $feedback->feedback }}</td>
                                            <td>{{ $feedback->user->name }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    <tfooter>
                                        <tr>
                                            <th >NO:</th>
                                            <th >Customer Name</th>
                                            <th >Customer Phone No</th>
                                            <th >Customer Rating</th>
                                            <th >Feedback</th>
                                            <th >Added By</th>
                                        </tr>
                                    </tfooter>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('scripts')

@endsection

